<!doctype html>
<html lang="pt-br">
<head>
    <?php include('inc_head.php'); ?>
</head>
<body>
    <?php include('inc_topo.php'); ?>

    <section id="banner" class="bg-cover overlay overlay-light pt-8 pt-lg-10 pb-7" style="background-image: url('assets/images/fundo-interna.jpg');">
        <div class="container text-center pt-8 pt-lg-10">
            <h1 class="display-1 d-inline-block text-white mt-6 mb-0 bg-primary px-2 py-1">CONHEÇA <span>LUGARES INCRÍVEIS</span> E FAÇA <span>NOVAS AMIZADES!</span></h1>
        </div>
    </section>

    <section class="py-7">
        <div class="container">
            <div class="row justify-content-center mb-6">
                <div class="col-md-10 col-lg-6 text-center">
                    <h3 class="title">Roteiros de viagem em grupo disponíveis</h3>
                    <p>Com as viagens em grupo, você e sua família podem fazer circuitos exclusivos e compartilhar as experiências com novos amigos!</p>
                </div>
            </div>
            <div class="row mb-6 scrollable-md">
                <div class="col-lg-4 ml-0 mb-4 mb-lg-6">
                    <a href="viagens-em-grupo-single.php" class="card">
                        <div class="ratio-img ratio-img-16by9">
                            <img src="assets/images/item-viagens-em-grupo.jpg" alt="Estados Unidos - Abril 2020" title="Estados Unidos - Abril 2020">
                        </div>
                        <div class="card-body bg-primary text-white text-center pt-6 px-4 pb-7">
                            <h3 class="title text-secondary mb-0">Estados Unidos</h3>
                            <strong class="d-block mb-2">Abril 2020</strong>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <span class="btn btn-center-bottom btn-secondary">Acesse o roteiro</span>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 ml-0 mb-4 mb-lg-6">
                    <a href="viagens-em-grupo-single.php" class="card">
                        <div class="ratio-img ratio-img-16by9">
                            <img src="assets/images/item-viagens-em-grupo.jpg" alt="Estados Unidos - Abril 2020" title="Estados Unidos - Abril 2020">
                        </div>
                        <div class="card-body bg-primary text-white text-center pt-6 px-4 pb-7">
                            <h3 class="title text-secondary mb-0">Estados Unidos</h3>
                            <strong class="d-block mb-2">Abril 2020</strong>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <span class="btn btn-center-bottom btn-secondary">Acesse o roteiro</span>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 ml-0 mb-4 mb-lg-6">
                    <a href="viagens-em-grupo-single.php" class="card">
                        <div class="ratio-img ratio-img-16by9">
                            <img src="assets/images/item-viagens-em-grupo.jpg" alt="Estados Unidos - Abril 2020" title="Estados Unidos - Abril 2020">
                        </div>
                        <div class="card-body bg-primary text-white text-center pt-6 px-4 pb-7">
                            <h3 class="title text-secondary mb-0">Estados Unidos</h3>
                            <strong class="d-block mb-2">Abril 2020</strong>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <span class="btn btn-center-bottom btn-secondary">Acesse o roteiro</span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="row justify-content-center mb-6">
                <div class="col-md-6 text-center">
                    <h3 class="title">Serviços, alimentação, passeios e experiências all inclusive*</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                </div>
            </div>
            <div class="row justify-content-center mb-6 pb-3 pb-lg-0 scrollable-md scrollable-3">
                <div class="col-md-2 ml-0">
                    <div class="ratio-img ratio-img-1by1 rounded-circle bg-secondary box-shadow">
                        <i class="fad fa-route"></i>
                        <!-- <img src="assets/images/destino.jpg" alt="" title=""> -->
                    </div>
                </div>
                <div class="col-md-2 ml-0">
                    <div class="ratio-img ratio-img-1by1 rounded-circle bg-secondary box-shadow">
                        <i class="fad fa-route"></i>
                        <img src="assets/images/destino.jpg" alt="" title="">
                    </div>
                </div>
                <div class="col-md-2 ml-0">
                    <div class="ratio-img ratio-img-1by1 rounded-circle bg-secondary box-shadow">
                        <i class="fad fa-route"></i>
                        <!-- <img src="assets/images/destino.jpg" alt="" title=""> -->
                    </div>
                </div>
                <div class="col-md-2 ml-0">
                    <div class="ratio-img ratio-img-1by1 rounded-circle bg-secondary box-shadow">
                        <i class="fad fa-route"></i>
                        <img src="assets/images/destino.jpg" alt="" title="">
                    </div>
                </div>
                <div class="col-md-2 ml-0">
                    <div class="ratio-img ratio-img-1by1 rounded-circle bg-secondary box-shadow">
                        <i class="fad fa-route"></i>
                        <!-- <img src="assets/images/destino.jpg" alt="" title=""> -->
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="bg-primary text-white py-8 py-lg-9 mb-6 overflow-hidden">
        <div class="container">
            <div class="row justify-content-center mb-6">
                <div class="col-md-6 text-center">
                    <h3 class="title">Tours guiados e assistências exclusivas</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                </div>
            </div>
            <div class="slick-4-itens">
                <div class="px-2">
                    <div class="ratio-img ratio-img-1by1 rounded box-shadow">
                        <a href="assets/images/galeria-grande.jpg" data-fancybox="tours-guiados" data-thumb="assets/images/galeria-pequena.jpg"><img src="assets/images/galeria-pequena.jpg" alt="Tour guiado 1" title="Tour guiado 1"></a>
                    </div>
                </div>
                <div class="px-2">
                    <div class="ratio-img ratio-img-1by1 rounded box-shadow">
                        <a href="assets/images/galeria-grande.jpg" data-fancybox="tours-guiados" data-thumb="assets/images/galeria-pequena.jpg"><img src="assets/images/galeria-pequena.jpg" alt="Tour guiado 1" title="Tour guiado 1"></a>
                    </div>
                </div>
                <div class="px-2">
                    <div class="ratio-img ratio-img-1by1 rounded box-shadow">
                        <a href="assets/images/galeria-grande.jpg" data-fancybox="tours-guiados" data-thumb="assets/images/galeria-pequena.jpg"><img src="assets/images/galeria-pequena.jpg" alt="Tour guiado 1" title="Tour guiado 1"></a>
                    </div>
                </div>
                <div class="px-2">
                    <div class="ratio-img ratio-img-1by1 rounded box-shadow">
                        <a href="assets/images/galeria-grande.jpg" data-fancybox="tours-guiados" data-thumb="assets/images/galeria-pequena.jpg"><img src="assets/images/galeria-pequena.jpg" alt="Tour guiado 1" title="Tour guiado 1"></a>
                    </div>
                </div>
                <div class="px-2">
                    <div class="ratio-img ratio-img-1by1 rounded box-shadow">
                        <a href="assets/images/galeria-grande.jpg" data-fancybox="tours-guiados" data-thumb="assets/images/galeria-pequena.jpg"><img src="assets/images/galeria-pequena.jpg" alt="Tour guiado 1" title="Tour guiado 1"></a>
                    </div>
                </div>
                <div class="px-2">
                    <div class="ratio-img ratio-img-1by1 rounded box-shadow">
                        <a href="assets/images/galeria-grande.jpg" data-fancybox="tours-guiados" data-thumb="assets/images/galeria-pequena.jpg"><img src="assets/images/galeria-pequena.jpg" alt="Tour guiado 1" title="Tour guiado 1"></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="depoimentos" class="pt-6 z-1 mb-n7 overflow-hidden">
        <div class="container position-relative">
            <div class="row justify-content-center">
                <div class="col-xl-10 text-center">
                    <h4 class="title mb-4">Quem viajou recomenda</h4>
                    <div class="slick-depoimentos">
                        <div class="d-flex align-items-stretch flex-column flex-lg-row bg-primary text-secondary rounded box-shadow overflow-hidden text-left">
                            <div class="col-lg-6 bg-cover py-9 py-lg-0" style="background-image: url('assets/images/depoimento.jpg');"></div>
                            <div class="col-lg-6 pt-6 px-4 pb-4 p-lg-6">
                                <div class="font-italic mb-3">
                                    <p>“Lorem ipsum dolor sit, amet consectetur adipisicing elit. Inventore, laborum quis distinctio obcaecati voluptates vero dolorem pariatur quos iusto fugit maiores voluptas nobis maxime ab est at ducimus necessitatibus! Similique. ipsum dolor sit amet consectetur adipisicing elit. Quo harum doloribus, fugiat nam magnam impedit explicabo quod cumque, numquam soluta nemo voluptates laboriosam molestiae, dignissimos voluptas repudiandae maxime et atque.”</p>
                                </div>
                                <strong>Renata Ramos, grupo de viagens Europa 2019</strong>
                            </div>
                        </div>
                        <div class="d-flex align-items-stretch flex-column flex-lg-row bg-primary text-secondary rounded box-shadow overflow-hidden text-left">
                            <div class="col-lg-6 bg-cover py-9 py-lg-0" style="background-image: url('assets/images/depoimento.jpg');"></div>
                            <div class="col-lg-6 pt-6 px-4 pb-4 p-lg-6">
                                <div class="font-italic mb-3">
                                    <p>“Lorem ipsum dolor sit, amet consectetur adipisicing elit. Inventore, laborum quis distinctio obcaecati voluptates vero dolorem pariatur quos iusto fugit maiores voluptas nobis maxime ab est at ducimus necessitatibus! Similique. ipsum dolor sit amet consectetur adipisicing elit. Quo harum doloribus, fugiat nam magnam impedit explicabo quod cumque, numquam soluta nemo voluptates laboriosam molestiae, dignissimos voluptas repudiandae maxime et atque.”</p>
                                </div>
                                <strong>Renata Ramos, grupo de viagens Europa 2019</strong>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php include('inc_rodape.php'); ?>
</body>
</html>