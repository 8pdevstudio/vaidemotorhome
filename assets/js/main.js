$(window).load(function() {
  if($('body').hasClass('loading')){
    setTimeout(function(){
      $('body').addClass('loaded').removeClass('loading');

    }, 500);
  }
  // Ratio Img
  $('.ratio-img img').each(function(){
    var img_w = this.width,
        img_h = this.height,
        container_w = $(this).parent('.ratio-img').width(),
        container_h = $(this).parent('.ratio-img').height();

    var img_class = (img_w / img_h < container_w / container_h ? 'tall' : 'wide');
    $(this).addClass(img_class);
  });
});

$(document).ready(function(){
  
  // Custom Select
  $('select.cs-select').each(function(index, el){
    new SelectFx(el);
  });

  // Custom Input Date
  Flatpickr = function() {
    var input = $('input[type="date"]');
    input.length && input.each(function() {
        var input, props, has_time;
        input = $(this);
        has_time = input.is('[data-time]');
        props = {
            dateFormat: has_time ? 'd/m/Y, H:i' : 'd/m/Y',
            time_24hr: true,
            enableTime: has_time ? true : false,
            disableMobile: false,
            locale: {
                firstDayOfWeek: 1,
                rangeSeparator: " até ",
                weekdays: {
                  shorthand: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
                  longhand: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],         
                }, 
                months: {
                  shorthand: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
                  longhand: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                },
              },
        }, input.flatpickr(props)
    })
  }();

  // Fancybox
  $('a[data-fancybox]').fancybox({
    thumbs: {
      autoStart: true,
      axis: "x"
    },
    buttons: [
      "fullScreen",
      "thumbs",
      "close"
    ],
    btnTpl: {
      arrowLeft:
        '<button data-fancybox-prev class="fancybox-button fancybox-button--arrow_left" title="{{PREV}}">' +
        '<i class="fa fa-chevron-left"></i>' +
        "</button>",
        arrowRight:
        '<button data-fancybox-next class="fancybox-button fancybox-button--arrow_right" title="{{NEXT}}">' +
        '<i class="fa fa-chevron-right"></i>' +
        "</button>",
    },
    lang: "pt",
    i18n: {
      pt: {
        CLOSE: "Fechar",
        NEXT: "Próximo",
        PREV: "Anterior",
        ERROR: "Arquivo não encontrado <br/> Por favor, tente novamente mais tarde.",
        PLAY_START: "Começar slideshow",
        PLAY_STOP: "Pausar slideshow",
        FULL_SCREEN: "Tela cheia",
        THUMBS: "Miniaturas",
        DOWNLOAD: "Baixar",
        SHARE: "Compartilhar",
        ZOOM: "Ampliar"
      },
    }
  });

  // Nav link active
  var page = window.location.href.substring(window.location.href.lastIndexOf('/') + 1),
      nav_links = $('.header-top .nav-link');
      
  nav_links.removeClass('active');
  if(page == ''){
    $(nav_links[0]).addClass('active');
  }
  else{
    $(nav_links).each(function(index, el) {
      if ($(el).attr('href') == page) {
        $(el).addClass('active');
      }
    });
  }

  // Header top fixed class
  $(window).scroll(function() {
    // Sticky Header
    $(this).scrollTop() != 0 && $('.header-top').hasClass('sticky-top') ? $('.header-top').addClass('is-sticky') : $('.header-top').removeClass('is-sticky');
    //$(this).scrollTop() == 0 ? $('.header-top').removeClass('is-sticky') : null; 
  });

  // Fixed in parent
  if($(window).width() > 992){
    $('.fixed').stick_in_parent({offset_top: 112});
  }

  // Active on scroll
  function reveal(el){
    var offset = 0.5;
    var active_class = $(el).data('animation') != undefined ? $(el).data('animation') : 'fadeInUp';
    var delay = $(el).data('delay') == undefined ? 0 : $(el).data('delay') + 'ms';
    var win_offset = $(window).scrollTop();

    if ($(el).is('[data-offset]')) {
      offset = $(el).data('offset');
      if (typeof offset == 'string') {
        offset == 'bottom' ? offset = 1 : null;
        offset == 'top' ? offset = 0 : null;
      } else {
        offset < 0 ? offset = 0 : null;
        offset > 1 ? offset = 1 : null;
      }
    }

    var title_offset = $(el).offset().top;
    win_offset = win_offset + $(window).height();

    if(win_offset > title_offset + ($(el).height() * offset)){
      $(el).addClass(active_class + ' animated').css('animationDelay', delay);
    } else if($(el).data('repeat')){
      $(el).removeClass(active_class + ' animated').css('animationDelay', 0);
    }
  }
  $('.reveal').each(function(index, el) {
    reveal(el);
  });
  $(window).scroll(function(){
    $('.reveal').each(function(index, el) {
      reveal(el);
    });
  });

  // SMOOTH SCROLLING - Code stolen from css-tricks for smooth scrolling:
  var scroll_offset = 96;
  $('a[href*=#]:not([href=#]):not([data-toggle]):not(.botao-tab)').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top - scroll_offset //ALTURA DO TOPO
        }, 1000, 'easeInOutExpo');
        if ($('#check-nav').is(':checked')) {
          $('#check-nav').click();
        }
        return false;
      }
    }
  });
  $('a[href=#][onclick]').click(function(event) {
    event.preventDefault();
    return false;
  });

  // Filtro
  if ($('.custom-nav').length > 0) {
    $('.custom-nav').each(function(index, el) {
      var filtros_ativos = [];
      var is_filtro = $(el).is('[data-filter]');   
      $(el).find('input').change(function() {
        var input_checked = '.' + this.value;

        if(this.type == 'radio'){
          $(this).parent().siblings().removeClass('active');
          $(this).parent().addClass('active');
          if(is_filtro){
            var filtro_target = $(el).data('filter');
            input_checked == '.tudo' ? $(filtro_target).slick('slickUnfilter') : $(filtro_target).slick('slickUnfilter').slick('slickFilter', input_checked);
            $(filtro_target).find('.slick-slide').addClass('no-transition');
            setTimeout(function() {
              $(filtro_target).find('.slick-slide').removeClass('no-transition');
            },100);
          }
        }
        else if(this.type == 'checkbox'){
          if($(this).is(':checked')){
            $(this).parent().addClass('active');
            filtros_ativos.push(input_checked);
          }
          else{
            filtros_ativos.splice(filtros_ativos.indexOf(input_checked),1);
            $(this).parent().removeClass('active');
          }

          if(is_filtro){
            $(filtro_target).slick('slickUnfilter').slick('slickFilter', filtros_ativos.toString());
          }
        }
        //console.log(filtros_ativos.toString());
      });
    });
  }

  // Input number
  $('.input-number button').click(function() {
      var button_is_plus = $(this).hasClass('btn-plus') ? true : false,
          input_number = $(this).parent().siblings('input[type="number"]'),
          data_increment = input_number.data('increment') != null ? Number($('#' + input_number.data('increment')).html()) : null,
          min_val = input_number.attr('min') != null ? input_number.attr('min') : 0,
          new_val = button_is_plus ? Number(input_number.val()) + 1 : Number(input_number.val()) - 1;

      input_number.val((new_val >= min_val ? new_val : min_val));

      if(input_number.data('increment') != null && new_val >= min_val){
        $('#' + input_number.data('increment')).html(button_is_plus ? data_increment + 1 : data_increment - 1);
      }
  });

  // Input slider range
  $('.slider-range').each(function (index, element) {
    var min_val = Number($(element).attr('min')) || 0;
    var max_val = Number($(element).attr('max')) || 100;
    var step = Number($(element).attr('step')) || 1;
    $(element).slider({
      range: true,
      animate: 'fast',
      min: min_val,
      max: max_val,
      step: step,
      values: [min_val, max_val],
      slide: function( event, ui ) {
        $(element).find('.ui-slider-handle').first().attr('data-val', ui.values[0]);
        $(element).find('.ui-slider-handle').last().attr('data-val', ui.values[1]);
      }
    });
    $(element).find('.ui-slider-handle').first().attr('data-val', $(element).slider('values', 0));
    $(element).find('.ui-slider-handle').last().attr('data-val', $(element).slider('values', 1));
  });

    
  // fecha  popup no esc
  $(document).keyup(function(e) {
      if (e.keyCode == 27) {
        $('#check-nav:checked,#check-search:checked').click();
      }
  });
  $('#check-nav:not(:checked)').change(function() {
    $('body').toggleClass('menu-open');
  });

  $('.mask-tel').focusout(function(){
      var phone, element;
      element = $(this);
      element.unmask();
      phone = element.val().replace(/\D/g, '');
      if(phone.length > 10) {
          element.mask("(99) 99999-999?9");
      } else {
          element.mask("(99) 9999-9999?9");
      }
  }).trigger('focusout');

  $('.mask-data').focusout(function(){
      var element;
      element = $(this);
      element.unmask();
      element.mask("99/99/9999");
  }).trigger('focusout');

  $('.mask-cep').focusout(function(){
      var element;
      element = $(this);
      element.unmask();
      element.mask("99999-999");
  }).trigger('focusout');

  $('.mask-cpf').focusout(function(){
      var element;
      element = $(this);
      element.unmask();
      $(this).mask("999.999.999-99");
  }).trigger('focusout');

  $('.mask-cnpj').focusout(function(){
      var element;
      element = $(this);
      element.unmask();
      $(this).mask("99.999.999/9999-99");
  }).trigger('focusout');

  if($('.slick-depoimentos').length > 0){
    $('.slick-depoimentos').slick({
      dots: false,
      arrows: true,
      infinite: false,
      loop: false,
      speed: 400,
      slidesToShow: 1,
      slidesToScroll: 1,
      adaptiveHeight: true,
      responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 1
          }
        }
      ]
    });
  }

  if($('.slick-4-itens').length > 0){
    $('.slick-4-itens').slick({
      dots: false,
      arrows: true,
      infinite: false,
      loop: false,
      speed: 200,
      slidesToShow: 4,
      slidesToScroll: 1,
      adaptiveHeight: true,
      responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 3
          }
        },
        {
          breakpoint: 568,
          settings: {
            slidesToShow: 2
          }
        },
        {
          breakpoint: 450,
          settings: {
            slidesToShow: 1
          }
        }
      ]
    });
  }

  if($('.galeria-grande').length > 0){
    $('.galeria-grande').slick({
      dots: false,
      arrows: false,
      infinite: true,
      loop: true,
      speed: 200,
      slidesToShow: 1,
      slidesToScroll: 1,
      adaptiveHeight: true,
      fade: true,
      asNavFor: '.galeria-pequena',
    });
  }
  if($('.galeria-pequena').length > 0){
    $('.galeria-pequena').slick({
      dots: false,
      arrows: true,
      infinite: true,
      loop: true,
      speed: 200,
      slidesToShow: 5,
      slidesToScroll: 1,
      adaptiveHeight: true,
      focusOnSelect: true,
      centerMode: true,
      asNavFor: '.galeria-grande',
      swipeToSlide: true,
      responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 3
          }
        }
      ]
    });
  }
});

function slickNext(slider){
  $(slider).slick('slickNext');
  return false;
}
function slickPrev(slider){
  $(slider).slick('slickPrev');
  return false;
}
function slickGoTo(slider,slide){
  $(slider).slick('slickGoTo',slide);
  return false;
}