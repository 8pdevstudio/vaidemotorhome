<!doctype html>
<html lang="pt-br">
<head>
    <?php include('inc_head.php'); ?>
</head>
<body>
    <?php include('inc_topo.php'); ?>

    <section class="py-4 py-md-7 sem-contato-rodape">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-lg-3 text-center">
                    <div class="fixed">
                        <div class="border border-primary border-width-2 bg-white p-1 fz-0 d-inline-block rounded-circle mb-2">
                            <div class="ratio-img ratio-img-avatar-xl bg-secondary">
                                <i class="fa fa-user-alt"></i>
                                <img src="assets/images/depoimento.jpg" alt="Nome Sobrenome" title="Nome Sobrenome">
                            </div>
                        </div>
                        <h1 class="title">Nome Sobrenome</h1>
                        <a href="#dados-pessoais" class="btn btn-link">Dados pessoais</a>
                        <br>
                        <a href="#dados-de-pagamento" class="btn btn-link">Dados de pagamento</a>
                        <br>
                        <a href="#meus-pedidos" class="btn btn-link">Meus pedidos</a>
                        <br>
                        <a href="#minhas-avaliacoes" class="btn btn-link">Minhas avaliações</a>
                    </div>
                </div>
                <div class="col-md-8 col-lg-9 mt-6 mt-md-0">
                    <form id="dados-pessoais">
                        <h2 class="title text-center text-md-left">Dados pessoais</h2>
                        <div class="row mx-n1">
                            <div class="col-md-6 p-1">
                                <div class="form-group">
                                    <input type="text" class="form-control border-light" placeholder="Nome completo" required>
                                </div>
                            </div>
                            <div class="col-md-6 p-1">
                                <div class="form-group">
                                    <input type="email" class="form-control border-light" placeholder="E-mail" required>
                                </div>
                            </div>
                            <div class="col-md-12 col-lg-4 p-1">
                                <div class="form-group">
                                    <input type="text" class="form-control border-light" placeholder="Endereço" required>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-4 col-xl-3 p-1">
                                <div class="form-group">
                                    <input type="text" class="form-control border-light" placeholder="Cidade" required>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-4 col-xl-3 p-1">
                                <div class="form-group">
                                    <select class="cs-select cs-select--wrap border-light maw-100">
                                        <option value="" disabled selected>Estado</option>
                                        <option value="opcao1">São Paulo</option>
                                        <option value="opcao2">Bahia</option>
                                        <option value="opcao2">Minas Gerais</option>
                                        <option value="opcao3">Mato Grosso do Sul</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-4 col-xl-2 p-1">
                                <div class="form-group">
                                    <input type="tel" class="form-control border-light mask-cep" placeholder="CEP" required>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-4 p-1">
                                <div class="form-group">
                                    <label class="label-abs d-lg-none">Data de nascimento</label>
                                    <input type="date" class="form-control border-light" placeholder="Nascimento" />
                                    <i class="far fa-calendar-alt form-icon text-primary"></i>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-4 p-1">
                                <div class="form-group">
                                    <input type="tel" class="form-control border-light mask-tel" placeholder="Celular" required>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-4 p-1">
                                <div class="form-group">
                                    <input type="tel" class="form-control border-light mask-tel" placeholder="Telefone" required>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-4 p-1">
                                <div class="form-group">
                                    <input type="text" class="form-control border-light" placeholder="RG" required>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-4 p-1">
                                <div class="form-group">
                                    <input type="text" class="form-control border-light" placeholder="CPF" required>
                                </div>
                            </div>
                            <div class="col-md-8 col-xl-4 p-1">
                                <button type="submit" class="btn btn-block btn-secondary">Alterar informações</a>
                            </div>
                        </div>
                    </form>
                    <hr>
                    <form id="dados-de-pagamento">
                        <h2 class="title">Dados de pagamento</h2>
                        <div class="row mx-n1 align-items-center">
                            <div class="col-12 p-1">
                                <div class="custom-control custom-radio d-inline-block">
                                    <input type="radio" class="custom-control-input" name="cartao" id="cartao-cc">
                                    <label class="custom-control-label" for="cartao-cc">Cartão de crédito</label>
                                </div>
                                <div class="custom-control custom-radio d-sm-inline-block mt-2 mt-sm-0 ml-sm-4">
                                    <input type="radio" class="custom-control-input" name="cartao" id="cartao-cd">
                                    <label class="custom-control-label" for="cartao-cd">Cartão de débito</label>
                                </div>
                            </div>
                            <div class="col-md-6 p-1">
                                <div class="form-group">
                                    <input type="text" class="form-control border-light" placeholder="Número do cartão" required>
                                </div>
                            </div>
                            <div class="col-md-6 p-1">
                                <div class="form-group">
                                    <input type="text" class="form-control border-light" placeholder="Nome no cartão" required>
                                </div>
                            </div>
                            <div class="col-md-4 col-xl-3 p-1">
                                <div class="form-group">
                                    <input type="text" class="form-control border-light" placeholder="Mês de validade" required>
                                </div>
                            </div>
                            <div class="col-md-4 col-xl-3 p-1">
                                <div class="form-group">
                                    <input type="text" class="form-control border-light" placeholder="Ano de validade" required>
                                </div>
                            </div>
                            <div class="col-md-4 col-xl-2 p-1">
                                <div class="form-group">
                                    <input type="text" class="form-control border-light" placeholder="Código" required>
                                </div>
                            </div>
                            <div class="col-md-12 col-xl-4 p-1">
                                <button type="submit" class="btn px-3 px-sm-4 font-size-base btn-secondary">Alterar</a>
                                <button type="submit" class="btn px-3 px-sm-4 font-size-base btn-secondary ml-2">Adicionar cartão</a>
                            </div>
                        </div>
                    </form>
                    <hr>
                    <div id="meus-pedidos">
                        <h2 class="title">Histórico de pedidos</h2>
                        <div class="item-motorhome item-motorhome-pedidos overflow-hidden py-2 py-xl-4 pl-xl-2 px-0 mt-2 small">
                            <div class="col-md-6 col-xl-3 border-right border-sm">
                                <span class="d-block">Aluguel de motorhome</span>
                                <h2 class="title mb-0">AS50 Campervan</h2>
                            </div>
                            <div class="col-md-6 col-xl-3 border-right border-sm">
                                <div class="d-flex justify-content-center justify-content-md-start">
                                    <i class="far fa-fw fa-calendar-alt font-size-base mr-1"></i>
                                    <div class="col-auto col-md px-0">
                                        <strong>Data da compra: </strong>
                                        <span>06/06/2020</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xl-2 border-right border-sm">
                                <strong>Nº do pedido: </strong>
                                <span>057810-4</span>
                            </div>
                            <div class="col-md-6 col-xl-2">
                                <button type="button" class="btn btn-link btn-sm">Ver detalhes<i class="fad fa-plus-circle text-secondary ml-1"></i></button>
                            </div>
                            <div class="col-xl-2 bg-primary text-white text-center align-self-stretch mt-2 my-xl-n4 mb-n2 py-2">
                                <span class="text-secondary">Status</span>
                                <strong class="d-block font-size-base">Finalizada</strong>
                                <hr class="my-2">
                                <a href="" class="btn btn-link text-white">Relatar erro</a>
                            </div>
                        </div>
                        <div class="item-motorhome item-motorhome-pedidos overflow-hidden py-2 py-xl-4 pl-xl-2 px-0 mt-2 small">
                            <div class="col-md-6 col-xl-3 border-right border-sm">
                                <span class="d-block">Aluguel de motorhome</span>
                                <h2 class="title mb-0">AS50 Campervan</h2>
                            </div>
                            <div class="col-md-6 col-xl-3 border-right border-sm">
                                <div class="d-flex justify-content-center justify-content-md-start">
                                    <i class="far fa-fw fa-calendar-alt font-size-base mr-1"></i>
                                    <div class="col-auto col-md px-0">
                                        <strong>Data da compra: </strong>
                                        <span>06/06/2020</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xl-2 border-right border-sm">
                                <strong>Nº do pedido: </strong>
                                <span>057810-4</span>
                            </div>
                            <div class="col-md-6 col-xl-2">
                                <button type="button" class="btn btn-link btn-sm">Ver detalhes<i class="fad fa-plus-circle text-secondary ml-1"></i></button>
                            </div>
                            <div class="col-xl-2 bg-primary text-white text-center align-self-stretch mt-2 my-xl-n4 mb-n2 py-2">
                                <span class="text-secondary">Status</span>
                                <strong class="d-block font-size-base">Finalizada</strong>
                                <hr class="my-2">
                                <a href="" class="btn btn-link text-white">Relatar erro</a>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div id="minhas-avaliacoes">
                        <h2 class="title">Minhas avaliações</h2>
                        <ul class="scrollable-sm">
                            <li class="border border-light p-2 p-sm-4 rounded my-md-2">
                                <strong class="font-size-lg">Renan Souza - "Excelente!"</strong>
                                <strong class="d-block mb-2">alugou no dia 12/10/2019</strong>
                                <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Tempora recusandae quis aspernatur maiores eveniet perferendis voluptas nisi, ut doloremque corrupti beatae consequuntur provident voluptatibus repellendus. Commodi molestias laudantium et consequuntur!</p>
                            </li>
                            <li class="border border-light p-2 p-sm-4 rounded my-md-2">
                                <strong class="font-size-lg">Renan Souza - "Excelente!"</strong>
                                <strong class="d-block mb-2">alugou no dia 12/10/2019</strong>
                                <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Tempora recusandae quis aspernatur maiores eveniet perferendis voluptas nisi, ut doloremque corrupti beatae consequuntur provident voluptatibus repellendus. Commodi molestias laudantium et consequuntur!</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php include('inc_rodape.php'); ?>
</body>
</html>