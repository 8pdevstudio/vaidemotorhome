<!doctype html>
<html lang="pt-br">
<head>
    <?php include('inc_head.php'); ?>
</head>
<body>
    <?php include('inc_topo.php'); ?>

    <section id="banner" class="bg-cover overlay overlay-light pt-8 pt-lg-10 pb-7" style="background-image: url('assets/images/fundo-interna.jpg');">
        <div class="container text-center pt-8 pt-lg-10">
            <h1 class="display-1 d-inline-block text-white mt-6 mb-0 bg-primary px-2 py-1">VIVA UMA <span>EXPERIÊNCIA ÚNICA</span> VIAJANDO DE MOTORHOME PELA EUROPA!</h1>
        </div>
    </section>

    <section class="py-7 sem-contato-rodape">
        <div class="container">
            <div class="row justify-content-center mb-7">
                <div class="col-md-10 col-lg-8 col-xl-6 text-center">
                    <h3 class="title">Circuito em três países (Alemanha, Áustria e Eslovênia) durante 11 dias e 10 noites</h3>
                    <p>Nosso roteiro de viagem inclui visitas em diferentes pontos turísticos, como restaurantes, SPA, museus e cidades históricas, experiência intensa com a natureza, com circuitos e atividades em parques, lagos e hospedagens em campings.</p>
                </div>
            </div>
            <div class="row flex-lg-row-reverse">
                <div class="col-lg-4 col-xl-3">
                    <div class="fixed pb-4">
                        <div class="bg-secondary rounded box-shadow p-4">
                            <h3 class="title text-center mb-0">Europa 2020</h3>
                            <hr class="border-primary mt-3 mb-4">
                            <div class="row">
                                <div class="col-auto">
                                    <i class="far fa-fw fa-calendar-alt font-size-lg"></i>
                                </div>
                                <div class="col pl-0">
                                    <strong>Retirada:</strong>
                                    <span>18/05/2020</span>
                                    <p>Sulzemoos, Alemanha</p>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-auto">
                                    <i class="far fa-fw fa-calendar-alt font-size-lg"></i>
                                </div>
                                <div class="col pl-0">
                                    <strong>Entrega:</strong>
                                    <span>28/05/2020</span>
                                    <p>Sulzemoos, Alemanha</p>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-auto">
                                    <i class="fa fa-fw fa-map-marker-alt font-size-lg"></i>
                                </div>
                                <div class="col pl-0">
                                    <strong>Percurso total:</strong>
                                    <span>2000 km</span>
                                </div>
                            </div>
                        </div>
                        <ul class="text-center pt-6 scrollable-md">
                            <li class="px-1 px-lg-0 mt-6">
                                <div class="ratio-img ratio-img-avatar-lg bg-secondary box-shadow rounded-circle">
                                    <i class="fad fa-utensils-alt"></i>
                                </div>
                                <h3 class="title mt-1">Restaurantes 5 estrelas</h3>
                            </li>
                            <li class="px-1 px-lg-0 mt-6">
                                <div class="ratio-img ratio-img-avatar-lg bg-secondary box-shadow rounded-circle">
                                    <i class="fad fa-landmark"></i>
                                </div>
                                <h3 class="title mt-1">Museus e Parques</h3>
                            </li>
                            <li class="px-1 px-lg-0 mt-6">
                                <div class="ratio-img ratio-img-avatar-lg bg-secondary box-shadow rounded-circle">
                                    <i class="fad fa-campground"></i>
                                </div>
                                <h3 class="title mt-1">Camping Parks</h3>
                            </li>
                            <li class="px-1 px-lg-0 mt-6">
                                <div class="ratio-img ratio-img-avatar-lg bg-secondary box-shadow rounded-circle">
                                    <i class="fad fa-map-marker-plus"></i>
                                </div>
                                <h3 class="title mt-1">Atividades extras</h3>
                            </li>
                            <li class="px-1 px-lg-0 mt-6">
                                <div class="ratio-img ratio-img-avatar-lg bg-secondary box-shadow rounded-circle">
                                    <i class="fad fa-mosque"></i>
                                </div>
                                <h3 class="title mt-1">Cidades históricas</h3>
                            </li>
                        </ul>
                        <div class="text-center d-none d-lg-block">
                            <hr class="my-6">
                            <h4 class="title mb-4">Ficou alguma dúvida? <br>Fale conosco pelo chat!</h4>
                            <a href="contato.php" class="btn btn-big-icon btn-primary">Entrar em contato</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-xl-9 mt-2 mt-lg-0">
                    <div class="galeria">
                        <div class="galeria-grande">
                            <div>
                                <a href="assets/images/galeria-grande.jpg" data-thumb="assets/images/galeria-pequena.jpg" data-fancybox="galeria" class="ratio-img ratio-img-16by9">
                                    <img src="assets/images/galeria-grande.jpg" alt="Foto 1" title="Foto 1">
                                </a>
                            </div>
                            <div>
                                <a href="assets/images/bg-destinos.jpg" data-thumb="assets/images/bg-destinos.jpg" data-fancybox="galeria" class="ratio-img ratio-img-16by9">
                                    <img src="assets/images/bg-destinos.jpg" alt="Foto 1" title="Foto 1">
                                </a>
                            </div>
                            <div>
                                <a href="assets/images/fundo-interna.jpg" data-thumb="assets/images/fundo-interna.jpg" data-fancybox="galeria" class="ratio-img ratio-img-16by9">
                                    <img src="assets/images/fundo-interna.jpg" alt="Foto 1" title="Foto 1">
                                </a>
                            </div>
                            <div>
                                <a href="assets/images/quem-somos.jpg" data-thumb="assets/images/quem-somos.jpg" data-fancybox="galeria" class="ratio-img ratio-img-16by9">
                                    <img src="assets/images/quem-somos.jpg" alt="Foto 1" title="Foto 1">
                                </a>
                            </div>
                            <div>
                                <a href="assets/images/vantagens.jpg" data-thumb="assets/images/vantagens.jpg" data-fancybox="galeria" class="ratio-img ratio-img-16by9">
                                    <img src="assets/images/vantagens.jpg" alt="Foto 1" title="Foto 1">
                                </a>
                            </div>
                            <div>
                                <a href="assets/images/motorhome.jpg" data-thumb="assets/images/motorhome.jpg" data-fancybox="galeria" class="ratio-img ratio-img-16by9">
                                    <img src="assets/images/motorhome.jpg" alt="Foto 1" title="Foto 1">
                                </a>
                            </div>
                        </div>
                        <div class="galeria-pequena">
                            <div class="mx-1 mx-sm-2">
                                <div class="ratio-img ratio-img-16by9">
                                    <img src="assets/images/galeria-pequena.jpg" alt="Foto 1" title="Foto 1">
                                </div>
                            </div>
                            <div class="mx-1 mx-sm-2">
                                <div class="ratio-img ratio-img-16by9">
                                    <img src="assets/images/bg-destinos.jpg" alt="Foto 1" title="Foto 1">
                                </div>
                            </div>
                            <div class="mx-1 mx-sm-2">
                                <div class="ratio-img ratio-img-16by9">
                                    <img src="assets/images/fundo-interna.jpg" alt="Foto 1" title="Foto 1">
                                </div>
                            </div>
                            <div class="mx-1 mx-sm-2">
                                <div class="ratio-img ratio-img-16by9">
                                    <img src="assets/images/quem-somos.jpg" alt="Foto 1" title="Foto 1">
                                </div>
                            </div>
                            <div class="mx-1 mx-sm-2">
                                <div class="ratio-img ratio-img-16by9">
                                    <img src="assets/images/vantagens.jpg" alt="Foto 1" title="Foto 1">
                                </div>
                            </div>
                            <div class="mx-1 mx-sm-2">
                                <div class="ratio-img ratio-img-16by9">
                                    <img src="assets/images/motorhome.jpg" alt="Foto 1" title="Foto 1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="bg-primary text-white pt-2 px-2 pt-md-4 px-md-4 pb-2 rounded text-center mt-4">
                        <h3 class="title text-secondary">Guia turístico Europa 2020</h3>
                        <hr class="border-secondary">
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Atque debitis illum totam, vero reiciendis cumque molestias voluptatibus ullam provident nam quos aperiam, fugit non suscipit porro quasi quisquam corrupti temporibus?</p>
                        <div class="accordion-buttons d-flex justify-content-between mt-4">
                            <button class="btn" type="button" data-toggle="collapse" data-target="#guia-1" aria-expanded="true" aria-controls="guia-1">1. Informações Gerais</button>
                            <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#guia-2" aria-expanded="true" aria-controls="guia-2">2. Roteiro</button>
                            <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#guia-3" aria-expanded="true" aria-controls="guia-3">3. Mais informações</button>
                            <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#guia-4" aria-expanded="true" aria-controls="guia-4">4. Incluso no pacote</button>
                            <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#guia-5" aria-expanded="true" aria-controls="guia-5">5. Valores</button>
                            <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#guia-6" aria-expanded="true" aria-controls="guia-6">6. Faça sua viagem</button>
                        </div>
                    </div>
                    <div class="border border-light rounded p-2 mt-4">
                        <div id="guia-turistico" class="accordion-content py-2 pr-4 pl-2">
                            <div id="guia-1" class="collapse show" data-parent="#guia-turistico">
                                <h3 class="title">Lorem Ipsum</h3>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Expedita quis facere nam consectetur suscipit perferendis in autem, dolorum ipsum commodi labore culpa voluptas ullam cum adipisci minus voluptatem iste blanditiis.</p>
                                <div class="row my-2 mx-n1">
                                    <div class="col-sm-4 p-1">
                                        <a href="assets/images/destino.jpg" data-fancybox="galeria-texto" data-thumb="assets/images/destino.jpg" class="ratio-img ratio-img-4by3 rounded">
                                            <img src="assets/images/destino.jpg" alt="Título da imagem" title="Título da imagem">
                                        </a>
                                    </div>
                                    <div class="col-sm-4 p-1">
                                        <a href="assets/images/destino.jpg" data-fancybox="galeria-texto" data-thumb="assets/images/destino.jpg" class="ratio-img ratio-img-4by3 rounded">
                                            <img src="assets/images/destino.jpg" alt="Título da imagem" title="Título da imagem">
                                        </a>
                                    </div>
                                    <div class="col-sm-4 p-1">
                                        <a href="assets/images/destino.jpg" data-fancybox="galeria-texto" data-thumb="assets/images/destino.jpg" class="ratio-img ratio-img-4by3 rounded">
                                            <img src="assets/images/destino.jpg" alt="Título da imagem" title="Título da imagem">
                                        </a>
                                    </div>
                                </div>
                                <h3 class="title">Lorem Ipsum</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam a turpis quis velit molestie lacinia nec a felis. Morbi dapibus augue non tempus tincidunt. Nunc eget vulputate nulla, ac porttitor quam. Vivamus finibus, justo eu lacinia vestibulum, justo tortor volutpat nibh, at suscipit nisl quam ut nulla. Nulla dui mi, pulvinar nec viverra in, varius quis magna. Vivamus dignissim purus lorem, ac volutpat tortor vestibulum bibendum. Fusce orci ligula, faucibus id vulputate sed, elementum ac ipsum. Mauris consectetur, neque eu ultricies mattis, nunc urna vehicula risus, quis gravida ligula metus sit amet risus. Praesent sit amet hendrerit sapien.</p>
                                <p>Cras in rutrum odio. Cras a odio a libero gravida aliquam. Sed vel sapien ligula. Praesent non nulla et diam varius pulvinar eget at sem. Donec non neque id nisl ullamcorper suscipit quis non massa. Vestibulum nibh ipsum, facilisis auctor rhoncus a, dignissim non nisi. Morbi eget egestas sapien.</p>
                            </div>
                            <div id="guia-2" class="collapse" data-parent="#guia-turistico">
                            2. Roteiro
                            </div>
                            <div id="guia-3" class="collapse" data-parent="#guia-turistico">
                            3. Mais informações
                            </div>
                            <div id="guia-4" class="collapse" data-parent="#guia-turistico">
                            4. Incluso no pacote
                            </div>
                            <div id="guia-5" class="collapse" data-parent="#guia-turistico">
                            5. Valores
                            </div>
                            <div id="guia-6" class="collapse" data-parent="#guia-turistico">
                            6. Faça sua viagem
                            </div>
                        </div>
                    </div>
                    <br>
                    <a href="viagens-em-grupo.php" class="btn btn-link btn-arrow-left">Voltar para a página de viagens em grupo</a>
                    <hr class="my-6">
                    <h3 class="title">Comentários e avaliações sobre o motorhome</h3>
                    <ul class="scrollable-md">
                        <li class="border border-light p-4 rounded px-2 my-lg-4">
                            <strong class="font-size-lg">Renan Souza - "Excelente!"</strong>
                            <strong class="d-block mb-2">alugou no dia 12/10/2019</strong>
                            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Tempora recusandae quis aspernatur maiores eveniet perferendis voluptas nisi, ut doloremque corrupti beatae consequuntur provident voluptatibus repellendus. Commodi molestias laudantium et consequuntur!</p>
                        </li>
                        <li class="border border-light p-4 rounded px-2 my-lg-4">
                            <strong class="font-size-lg">Renan Souza - "Excelente!"</strong>
                            <strong class="d-block mb-2">alugou no dia 12/10/2019</strong>
                            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Tempora recusandae quis aspernatur maiores eveniet perferendis voluptas nisi, ut doloremque corrupti beatae consequuntur provident voluptatibus repellendus. Commodi molestias laudantium et consequuntur!</p>
                        </li>
                        <li class="border border-light p-4 rounded px-2 my-lg-4">
                            <strong class="font-size-lg">Renan Souza - "Excelente!"</strong>
                            <strong class="d-block mb-2">alugou no dia 12/10/2019</strong>
                            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Tempora recusandae quis aspernatur maiores eveniet perferendis voluptas nisi, ut doloremque corrupti beatae consequuntur provident voluptatibus repellendus. Commodi molestias laudantium et consequuntur!</p>
                        </li>
                    </ul>
                    <hr class="my-6">
                    <h3 class="title text-center text-sm-left"><span class="d-block d-sm-inline-block">Deixe seu comentário!</span> <a href="login.php" class="btn btn-secondary mt-3 mt-sm-0 ml-sm-6 va-initial">Faça login</a></h3>
                    <form class="mt-4">
                        <div class="row mx-n1">
                            <div class="col-md-4 p-1">
                                <div class="form-group">
                                    <input type="text" class="form-control border-light" placeholder="Nome" required>
                                </div>
                            </div>
                            <div class="col-md-4 p-1">
                                <div class="form-group">
                                    <input type="text" class="form-control border-light" placeholder="Título" required>
                                </div>
                            </div>
                            <div class="col-md-4 p-1">
                                <div class="form-group">
                                    <input type="date" class="form-control border-light" placeholder="Data da compra" />
                                    <i class="far fa-calendar-alt form-icon text-primary"></i>
                                </div>
                            </div>
                            <div class="col-12 p-1">
                                <div class="form-group">
                                    <textarea class="form-control border-light" placeholder="Comentário" required></textarea>
                                </div>
                            </div>
                            <div class="col-md-4 p-1">
                                <button type="submit" class="btn btn-block btn-secondary">Postar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <?php include('inc_rodape.php'); ?>
</body>
</html>