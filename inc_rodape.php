    <section id="contato-rodape" class="py-6 py-lg-9 bg-light">
        <div class="container pt-6 pb-9">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6 text-center">
                    <h4 class="title mb-6">Ficou alguma dúvida? <br>Fale conosco pelo chat!</h4>
                    <a href="contato.php" class="btn btn-big-icon btn-primary">Entrar em contato</a>
                </div>
            </div>
        </div>
    </section>
    
    <footer class="bg-primary text-secondary">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-auto">
                    <img src="assets/images/logo-rodape.png" alt="Vai de Motorhome" title="Vai de Motorhome" class="img-fluid mt-n50p">
                </div>
            </div>
            <div class="row mt-6 pb-2 justify-content-between text-center text-md-left">
                <div class="col-md-4">
                    <h4 class="title mb-2 mb-md-3">Receba nossa newsletter</h4>
                    <p>Seja o primeiro a receber nossas novidades!</p>
                    <form class="row mx-n1">
                        <div class="col px-1">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Seu email" required />
                            </div>
                        </div>
                        <div class="col-auto col-md-12 col-lg-auto px-1 mt-md-2 mt-lg-0">
                            <div class="form-group">
                                <button type="submit" class="btn btn-block btn-secondary">Enviar</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-4 col-xl-3 mt-4 mt-md-0">
                    <h4 class="title mb-2 mb-md-3">Mapa do site</h4>
                    <div class="duas-colunas break-at-md">
                        <ul>
                            <li><a href="alugue.php">Alugue</a></li>
                            <li><a href="faq.php">FAQ</a></li>
                            <li><a href="viagens-em-grupo.php">Viagens em grupo</a></li>
                            <li><a href="blog.php">Blog</a></li>
                            <li><a href="consultoria.php">Consultoria</a></li>
                            <li><a href="minha-conta.php">Minha conta</a></li>
                            <li><a href="quem-somos.php">Sobre nós</a></li>
                            <li><a href="filiado.php">Seja um filiado</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4 col-xl-3 mt-4 mt-md-0">
                    <h4 class="title mb-2 mb-md-3">Contato</h4>
                    <a href="mailto:reservas@vaidemotorhome.com.br">reservas@vaidemotorhome.com.br</a>
                    <div class="row mx-n1 mt-4 justify-content-center justify-content-md-start">
                        <div class="col-auto p-1">
                            <a href="#" target="_blank" title="Instagram"><i class="fab fa-instagram font-size-xl"></i></a>
                        </div>
                        <div class="col-auto p-1">
                            <a href="#" target="_blank" title="Youtube"><i class="fab fa-youtube font-size-xl"></i></a>
                        </div>
                        <div class="col-auto p-1">
                            <a href="#" target="_blank" title="Facebook"><i class="fab fa-facebook font-size-xl"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="text-center pb-4">
                <span>2020 © Vai de Motorhome. Todos os direitos reservados. TAS Viagens e Turismo LTDA.</span>
            </div>
        </div>
    </footer>


    <script type="text/javascript" src="assets/js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.ui.touch-punch.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.easing.min.js"></script>
    <script type="text/javascript" src="assets/js/popper.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.js"></script>
    <script type="text/javascript" src="assets/js/slick.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.sticky-kit.min.js"></script>
    <script type="text/javascript" src="assets/js/fontsmoothie.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.maskedinput.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.fancybox.min.js"></script>
    <script type="text/javascript" src="assets/js/flatpickr.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script>

    <!-- Custom Select -->
    <script type="text/javascript" src="assets/js/classie.js"></script>
    <script type="text/javascript" src="assets/js/selectFx.js"></script>
    