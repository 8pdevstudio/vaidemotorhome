<!doctype html>
<html lang="pt-br">
<head>
    <?php include('inc_head.php'); ?>
</head>
<body>
    <?php include('inc_topo.php'); ?>

    <section id="banner" class="bg-cover overlay overlay-light pt-8 pt-lg-10 pb-7" style="background-image: url('assets/images/fundo-interna.jpg');">
        <div class="container text-center pt-8 pt-lg-10">
            <h1 class="display-1 d-inline-block text-white mt-6 mb-0 bg-primary px-2 py-1"><span>Página da</span> calculadora</h1>
        </div>
    </section>

    <section class="py-7">
    </section>

    <?php include('inc_rodape.php'); ?>
</body>
</html>