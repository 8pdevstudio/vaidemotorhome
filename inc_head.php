
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="theme-color" content="#396152">
    <meta name="msapplication-navbutton-color" content="#396152">
    <meta name="apple-mobile-web-app-status-bar-style" content="#396152">
    <title>Vai de Motorhome</title>

    <link rel="shortcut icon" href="assets/images/favicon.png">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,700|Zilla+Slab:700&display=swap">
    <link rel="stylesheet" href="assets/css/font-awesome.css" type="text/css">
    <link rel="stylesheet" href="assets/css/stylesheet.css" type="text/css">
    <link rel="stylesheet" href="assets/css/animate.css" type="text/css">
    <link rel="stylesheet" href="assets/css/slick.css" type="text/css"/>
    <link rel="stylesheet" href="assets/css/flatpickr.min.css" type="text/css"/>
    <link rel="stylesheet" href="assets/css/jquery.fancybox.min.css" type="text/css"/>
    <link rel="stylesheet" href="assets/css/jquery-ui.css" type="text/css"/>
    <link rel="stylesheet" href="assets/css/main.css" type="text/css">