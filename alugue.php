<!doctype html>
<html lang="pt-br">
<head>
    <?php include('inc_head.php'); ?>
</head>
<body>
    <?php include('inc_topo.php'); ?>

    <main class="pt-2 pt-md-6 pb-8">
        <div class="container">
            <div class="row align-items-stretch">
                <form class="col-lg-4 col-xl-3">
                    <div class="row justify-content-between d-lg-none border-bottom border-light pb-2 mb-3 mx-0 mx-sm-n2">
                        <div class="col-auto">
                            <label class="btn btn-link btn-arrow-left" for="side-modal-busca">Busca</label>
                        </div>
                        <div class="col-auto">
                            <label class="btn btn-link btn-arrow" for="side-modal-filtro">Filtro</label>
                        </div>
                    </div>
                    <input id="side-modal-busca" type="checkbox" class="d-none">
                    <div class="side-modal side-modal-left">
                        <label for="side-modal-busca" class="backdrop"></label>
                        <label for="side-modal-busca" class="close"><i class="far fa-times text-primary"></i></label>
                        <div class="content">
                            <div class="bg-primary text-white box-shadow rounded p-3 position-relative">
                                <div class="form-group">
                                    <select class="cs-select cs-select--wrap maw-100">
                                        <option value="" disabled selected>País</option>
                                        <option value="opcao1">Opção 1Opção 1Opção 1Opção 1Opção 1Opção 1Opção 1</option>
                                        <option value="opcao2">Opção 1</option>
                                        <option value="opcao2">Opção 2</option>
                                        <option value="opcao3">Opção 3</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <select class="cs-select cs-select--wrap maw-100">
                                        <option value="" disabled selected>Retirar em</option>
                                        <option value="opcao1">Opção 1Opção 1Opção 1Opção 1Opção 1Opção 1Opção 1</option>
                                        <option value="opcao1">Opção 1</option>
                                        <option value="opcao2">Opção 2</option>
                                        <option value="opcao3">Opção 3</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <select class="cs-select cs-select--wrap maw-100">
                                        <option value="" disabled selected>Entregar em</option>
                                        <option value="opcao1">Opção 1Opção 1Opção 1Opção 1Opção 1Opção 1Opção 1</option>
                                        <option value="opcao1">Opção 1</option>
                                        <option value="opcao2">Opção 2</option>
                                        <option value="opcao3">Opção 3</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <input type="date" class="form-control" placeholder="Data da retirada" />
                                    <i class="far fa-calendar-alt form-icon"></i>
                                </div>
                                <div class="form-group">
                                    <input type="date" class="form-control" placeholder="Data da entrega" />
                                    <i class="far fa-calendar-alt form-icon"></i>
                                </div>
                                <div class="form-group">
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="possui-hab-int">
                                        <label class="custom-control-label" for="possui-hab-int">Possui habilitação internacional</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <select class="cs-select cs-select--wrap maw-100">
                                        <option value="" disabled selected>País da licença para dirigir</option>
                                        <option value="opcao1">Opção 1Opção 1Opção 1Opção 1Opção 1Opção 1Opção 1</option>
                                        <option value="opcao1">Opção 1</option>
                                        <option value="opcao2">Opção 2</option>
                                        <option value="opcao3">Opção 3</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control bg-white" value="Idade" readonly />
                                    <div class="form-icon">
                                        <div class="input-group input-number input-number-sm">
                                            <div class="input-group-prepend">
                                                <button class="btn text-primary btn-minus" type="button"><i class="far fa-minus"></i></button>
                                            </div>
                                            <input type="number" class="form-control" value="18" min="18" step="1" readonly>
                                            <div class="input-group-append">
                                                <button class="btn text-primary btn-plus" type="button"><i class="far fa-plus"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button class="btn btn-secondary btn-center-bottom" type="submit">Buscar</button>
                            </div>
                        </div>
                    </div>
                    <input id="side-modal-filtro" type="checkbox" class="d-none">
                    <div class="side-modal">
                        <label for="side-modal-filtro" class="backdrop"></label>
                        <label for="side-modal-filtro" class="close"><i class="far fa-times text-primary"></i></label>
                        <div class="content">
                            <div class="bg-light box-shadow rounded p-3 mb-6 mb-lg-0 mt-lg-8 position-relative">
                                <a class="btn btn-collapse" data-toggle="collapse" href="#collapse-passageiros" role="button" aria-expanded="true" aria-controls="collapse-passageiros">
                                    Nº de passageiros <i class="fa fa-chevron-up"></i>
                                </a>
                                <div class="collapse show" id="collapse-passageiros">
                                    <div class="pt-3 px-2 pb-4">
                                        <div class="slider-range" min="1" max="6"></div>
                                    </div>
                                </div>
                                <hr>
                                <a class="btn btn-collapse" data-toggle="collapse" href="#collapse-espacos" role="button" aria-expanded="true" aria-controls="collapse-espacos">
                                    Espaços <i class="fa fa-chevron-up"></i>
                                </a>
                                <div class="collapse mt-2 show" id="collapse-espacos">
                                    <div class="form-group">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="espaco-chuveiro">
                                            <label class="custom-control-label" for="espaco-chuveiro">Chuveiro</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="espaco-banheiro">
                                            <label class="custom-control-label" for="espaco-banheiro">Banheiro</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="espaco-cozinha">
                                            <label class="custom-control-label" for="espaco-cozinha">Cozinha</label>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <a class="btn btn-collapse" data-toggle="collapse" href="#collapse-preco" role="button" aria-expanded="true" aria-controls="collapse-preco">
                                    Faixa de preço (R$) <i class="fa fa-chevron-up"></i>
                                </a>
                                <div class="collapse show" id="collapse-preco">
                                    <div class="pt-3 px-2 pb-4">
                                        <div class="slider-range" min="200" max="2000" step="100"></div>
                                    </div>
                                </div>
                                <hr>
                                <button class="btn btn-secondary btn-center-bottom">Limpar filtro</button>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="col-lg-8 col-xl-9">
                    <div class="row">
                        <div class="col">
                            <h1 class="title mb-1">Resultado da busca (41)</h1>
                            <p>Retirar em <strong>Roma</strong> e deixar em <strong>Atena</strong></p>
                        </div>
                        <div class="col-auto">
                            <select class="cs-select secondary">
                                <option value="" disabled selected>Ordenar por</option>
                                <option value="relevancia">Relevância</option>
                                <option value="menorpreco">Menor preço</option>
                                <option value="maiorpreco">Maior preço</option>
                            </select>
                        </div>
                    </div>
                    <div class="item-motorhome mt-4">
                        <div class="col-md-8 col-xl-9 px-md-0 px-xl-2">
                            <div class="row align-items-center">
                                <div class="col-md-4 mb-3 mb-md-0">
                                    <div class="ratio-img ratio-img-4by3">
                                        <img src="assets/images/motorhome.jpg" alt="AS50 Campervan" title="AS50 Campervan">
                                    </div>
                                </div>
                                <div class="col text-center text-md-left">
                                    <h4 class="title">AS50 Campervan</h4>
                                    <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi dolore riatur repudiandae!</p>
                                </div>
                                <div class="col-12 col-xl-auto text-center text-xl-right mt-2 mt-xl-0 d-none d-md-block">
                                    <a href="assets/images/galeria-grande.jpg" data-fancybox="as50-campervan-2" data-thumb="assets/images/galeria-pequena.jpg" class="btn btn-link">Ver fotos<i class="fad fa-plus-circle text-secondary ml-1"></i></a>
                                    <div class="d-none">
                                        <a href="assets/images/galeria-grande.jpg" data-fancybox="as50-campervan-2" data-thumb="assets/images/galeria-pequena.jpg"></a>
                                        <a href="assets/images/galeria-grande.jpg" data-fancybox="as50-campervan-2" data-thumb="assets/images/galeria-pequena.jpg"></a>
                                        <a href="assets/images/galeria-grande.jpg" data-fancybox="as50-campervan-2" data-thumb="assets/images/galeria-pequena.jpg"></a>
                                        <a href="assets/images/galeria-grande.jpg" data-fancybox="as50-campervan-2" data-thumb="assets/images/galeria-pequena.jpg"></a>
                                    </div>
                                    <br class="d-none d-xl-block">
                                    <button type="button" class="btn btn-link ml-3 ml-xl-0" data-toggle="modal" data-target="#modal-espec">Especificações<i class="fad fa-plus-circle text-secondary ml-1"></i></button>
                                </div>
                            </div>
                            <hr>
                            <ul class="lista-detalhes row mx-n1">
                                <li class="col-6 col-sm-4 col-md-6 col-xl-3 px-1 d-flex">
                                    <i class="fa fa-fw fa-users mr-1"></i>
                                    <span>5 passageiros</span>
                                </li>
                                <li class="col-6 col-sm-4 col-md-6 col-xl-3 px-1 d-flex">
                                    <i class="fa fa-fw fa-steering-wheel mr-1"></i>
                                    <span>Manual</span>
                                </li>
                                <li class="col-6 col-sm-4 col-md-6 col-xl-3 px-1 d-flex">
                                    <i class="fa fa-fw fa-faucet-drip mr-1"></i>
                                    <span>Pia cozinha</span>
                                </li>
                                <li class="col-6 col-sm-4 col-md-6 col-xl-3 px-1 d-flex">
                                    <i class="fa fa-fw fa-snowflake mr-1"></i>
                                    <span>Ar condicionado</span>
                                </li>  
                                <li class="col-6 col-sm-4 col-md-6 col-xl-3 px-1 d-flex">
                                    <i class="fa fa-fw fa-bed-alt mr-1"></i>
                                    <span>5 camas</span>
                                </li>
                                <li class="col-6 col-sm-4 col-md-6 col-xl-3 px-1 d-flex">
                                    <i class="far fa-fw fa-th-large mr-1"></i>
                                    <span>Cooktop</span>
                                </li>
                                <li class="col-6 col-sm-4 col-md-6 col-xl-3 px-1 d-flex">
                                    <i class="fa fa-fw fa-refrigerator mr-1"></i>
                                    <span>Refrigerador</span>
                                </li>
                                <li class="col-6 col-sm-4 col-md-6 col-xl-3 px-1 d-flex">
                                    <i class="fa fa-fw fa-shower mr-1"></i>
                                    <span>Banheiro</span>
                                </li>   
                            </ul>
                        </div>
                        <div class="col-md-4 col-xl-3 pr-md-0">
                            <div class="bg-primary rounded box-shadow text-secondary text-center p-3 mb-2 mt-3 my-md-0 mr-md-n4 position-relative">
                                <h4 class="text-white title mb-0">R$450,00</h4>
                                <p>Incluso taxas e impostos</p>
                                <hr class="my-3">
                                <p>Diária: R$75,00</p>
                                <p>7 dias: R$450,00</p>
                                <a href="motorhome.php" class="btn btn-center-bottom btn-secondary">Ver mais</a>
                            </div>
                        </div>
                    </div>
                    <div class="item-motorhome mt-4">
                        <div class="col-md-8 col-xl-9 px-md-0 px-xl-2">
                            <div class="row align-items-center">
                                <div class="col-md-4 mb-3 mb-md-0">
                                    <div class="ratio-img ratio-img-4by3">
                                        <img src="assets/images/motorhome.jpg" alt="AS50 Campervan" title="AS50 Campervan">
                                    </div>
                                </div>
                                <div class="col text-center text-md-left">
                                    <h4 class="title">AS50 Campervan</h4>
                                    <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi dolore riatur repudiandae!</p>
                                </div>
                                <div class="col-12 col-xl-auto text-center text-xl-right mt-2 mt-xl-0 d-none d-md-block">
                                    <a href="assets/images/galeria-grande.jpg" data-fancybox="as50-campervan-2" data-thumb="assets/images/galeria-pequena.jpg" class="btn btn-link">Ver fotos<i class="fad fa-plus-circle text-secondary ml-1"></i></a>
                                    <div class="d-none">
                                        <a href="assets/images/galeria-grande.jpg" data-fancybox="as50-campervan-2" data-thumb="assets/images/galeria-pequena.jpg"></a>
                                        <a href="assets/images/galeria-grande.jpg" data-fancybox="as50-campervan-2" data-thumb="assets/images/galeria-pequena.jpg"></a>
                                        <a href="assets/images/galeria-grande.jpg" data-fancybox="as50-campervan-2" data-thumb="assets/images/galeria-pequena.jpg"></a>
                                        <a href="assets/images/galeria-grande.jpg" data-fancybox="as50-campervan-2" data-thumb="assets/images/galeria-pequena.jpg"></a>
                                    </div>
                                    <br class="d-none d-xl-block">
                                    <button type="button" class="btn btn-link ml-3 ml-xl-0" data-toggle="modal" data-target="#modal-espec">Especificações<i class="fad fa-plus-circle text-secondary ml-1"></i></button>
                                </div>
                            </div>
                            <hr>
                            <ul class="lista-detalhes row mx-n1">
                                <li class="col-6 col-sm-4 col-md-6 col-xl-3 px-1 d-flex">
                                    <i class="fa fa-fw fa-users mr-1"></i>
                                    <span>5 passageiros</span>
                                </li>
                                <li class="col-6 col-sm-4 col-md-6 col-xl-3 px-1 d-flex">
                                    <i class="fa fa-fw fa-steering-wheel mr-1"></i>
                                    <span>Manual</span>
                                </li>
                                <li class="col-6 col-sm-4 col-md-6 col-xl-3 px-1 d-flex">
                                    <i class="fa fa-fw fa-faucet-drip mr-1"></i>
                                    <span>Pia cozinha</span>
                                </li>
                                <li class="col-6 col-sm-4 col-md-6 col-xl-3 px-1 d-flex">
                                    <i class="fa fa-fw fa-snowflake mr-1"></i>
                                    <span>Ar condicionado</span>
                                </li>  
                                <li class="col-6 col-sm-4 col-md-6 col-xl-3 px-1 d-flex">
                                    <i class="fa fa-fw fa-bed-alt mr-1"></i>
                                    <span>5 camas</span>
                                </li>
                                <li class="col-6 col-sm-4 col-md-6 col-xl-3 px-1 d-flex">
                                    <i class="far fa-fw fa-th-large mr-1"></i>
                                    <span>Cooktop</span>
                                </li>
                                <li class="col-6 col-sm-4 col-md-6 col-xl-3 px-1 d-flex">
                                    <i class="fa fa-fw fa-refrigerator mr-1"></i>
                                    <span>Refrigerador</span>
                                </li>
                                <li class="col-6 col-sm-4 col-md-6 col-xl-3 px-1 d-flex">
                                    <i class="fa fa-fw fa-shower mr-1"></i>
                                    <span>Banheiro</span>
                                </li>   
                            </ul>
                        </div>
                        <div class="col-md-4 col-xl-3 pr-md-0">
                            <div class="bg-primary rounded box-shadow text-secondary text-center p-3 mb-2 mt-3 my-md-0 mr-md-n4 position-relative">
                                <h4 class="text-white title mb-0">R$450,00</h4>
                                <p>Incluso taxas e impostos</p>
                                <hr class="my-3">
                                <p>Diária: R$75,00</p>
                                <p>7 dias: R$450,00</p>
                                <a href="motorhome.php" class="btn btn-center-bottom btn-secondary">Ver mais</a>
                            </div>
                        </div>
                    </div>
                    <div class="item-motorhome mt-4">
                        <div class="col-md-8 col-xl-9 px-md-0 px-xl-2">
                            <div class="row align-items-center">
                                <div class="col-md-4 mb-3 mb-md-0">
                                    <div class="ratio-img ratio-img-4by3">
                                        <img src="assets/images/motorhome.jpg" alt="AS50 Campervan" title="AS50 Campervan">
                                    </div>
                                </div>
                                <div class="col text-center text-md-left">
                                    <h4 class="title">AS50 Campervan</h4>
                                    <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi dolore riatur repudiandae!</p>
                                </div>
                                <div class="col-12 col-xl-auto text-center text-xl-right mt-2 mt-xl-0 d-none d-md-block">
                                    <a href="assets/images/galeria-grande.jpg" data-fancybox="as50-campervan-2" data-thumb="assets/images/galeria-pequena.jpg" class="btn btn-link">Ver fotos<i class="fad fa-plus-circle text-secondary ml-1"></i></a>
                                    <div class="d-none">
                                        <a href="assets/images/galeria-grande.jpg" data-fancybox="as50-campervan-2" data-thumb="assets/images/galeria-pequena.jpg"></a>
                                        <a href="assets/images/galeria-grande.jpg" data-fancybox="as50-campervan-2" data-thumb="assets/images/galeria-pequena.jpg"></a>
                                        <a href="assets/images/galeria-grande.jpg" data-fancybox="as50-campervan-2" data-thumb="assets/images/galeria-pequena.jpg"></a>
                                        <a href="assets/images/galeria-grande.jpg" data-fancybox="as50-campervan-2" data-thumb="assets/images/galeria-pequena.jpg"></a>
                                    </div>
                                    <br class="d-none d-xl-block">
                                    <button type="button" class="btn btn-link ml-3 ml-xl-0" data-toggle="modal" data-target="#modal-espec">Especificações<i class="fad fa-plus-circle text-secondary ml-1"></i></button>
                                </div>
                            </div>
                            <hr>
                            <ul class="lista-detalhes row mx-n1">
                                <li class="col-6 col-sm-4 col-md-6 col-xl-3 px-1 d-flex">
                                    <i class="fa fa-fw fa-users mr-1"></i>
                                    <span>5 passageiros</span>
                                </li>
                                <li class="col-6 col-sm-4 col-md-6 col-xl-3 px-1 d-flex">
                                    <i class="fa fa-fw fa-steering-wheel mr-1"></i>
                                    <span>Manual</span>
                                </li>
                                <li class="col-6 col-sm-4 col-md-6 col-xl-3 px-1 d-flex">
                                    <i class="fa fa-fw fa-faucet-drip mr-1"></i>
                                    <span>Pia cozinha</span>
                                </li>
                                <li class="col-6 col-sm-4 col-md-6 col-xl-3 px-1 d-flex">
                                    <i class="fa fa-fw fa-snowflake mr-1"></i>
                                    <span>Ar condicionado</span>
                                </li>  
                                <li class="col-6 col-sm-4 col-md-6 col-xl-3 px-1 d-flex">
                                    <i class="fa fa-fw fa-bed-alt mr-1"></i>
                                    <span>5 camas</span>
                                </li>
                                <li class="col-6 col-sm-4 col-md-6 col-xl-3 px-1 d-flex">
                                    <i class="far fa-fw fa-th-large mr-1"></i>
                                    <span>Cooktop</span>
                                </li>
                                <li class="col-6 col-sm-4 col-md-6 col-xl-3 px-1 d-flex">
                                    <i class="fa fa-fw fa-refrigerator mr-1"></i>
                                    <span>Refrigerador</span>
                                </li>
                                <li class="col-6 col-sm-4 col-md-6 col-xl-3 px-1 d-flex">
                                    <i class="fa fa-fw fa-shower mr-1"></i>
                                    <span>Banheiro</span>
                                </li>   
                            </ul>
                        </div>
                        <div class="col-md-4 col-xl-3 pr-md-0">
                            <div class="bg-primary rounded box-shadow text-secondary text-center p-3 mb-2 mt-3 my-md-0 mr-md-n4 position-relative">
                                <h4 class="text-white title mb-0">R$450,00</h4>
                                <p>Incluso taxas e impostos</p>
                                <hr class="my-3">
                                <p>Diária: R$75,00</p>
                                <p>7 dias: R$450,00</p>
                                <a href="motorhome.php" class="btn btn-center-bottom btn-secondary">Ver mais</a>
                            </div>
                        </div>
                    </div>
                    <div class="item-motorhome mt-4">
                        <div class="col-md-8 col-xl-9 px-md-0 px-xl-2">
                            <div class="row align-items-center">
                                <div class="col-md-4 mb-3 mb-md-0">
                                    <div class="ratio-img ratio-img-4by3">
                                        <img src="assets/images/motorhome.jpg" alt="AS50 Campervan" title="AS50 Campervan">
                                    </div>
                                </div>
                                <div class="col text-center text-md-left">
                                    <h4 class="title">AS50 Campervan</h4>
                                    <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi dolore riatur repudiandae!</p>
                                </div>
                                <div class="col-12 col-xl-auto text-center text-xl-right mt-2 mt-xl-0 d-none d-md-block">
                                    <a href="assets/images/galeria-grande.jpg" data-fancybox="as50-campervan-2" data-thumb="assets/images/galeria-pequena.jpg" class="btn btn-link">Ver fotos<i class="fad fa-plus-circle text-secondary ml-1"></i></a>
                                    <div class="d-none">
                                        <a href="assets/images/galeria-grande.jpg" data-fancybox="as50-campervan-2" data-thumb="assets/images/galeria-pequena.jpg"></a>
                                        <a href="assets/images/galeria-grande.jpg" data-fancybox="as50-campervan-2" data-thumb="assets/images/galeria-pequena.jpg"></a>
                                        <a href="assets/images/galeria-grande.jpg" data-fancybox="as50-campervan-2" data-thumb="assets/images/galeria-pequena.jpg"></a>
                                        <a href="assets/images/galeria-grande.jpg" data-fancybox="as50-campervan-2" data-thumb="assets/images/galeria-pequena.jpg"></a>
                                    </div>
                                    <br class="d-none d-xl-block">
                                    <button type="button" class="btn btn-link ml-3 ml-xl-0" data-toggle="modal" data-target="#modal-espec">Especificações<i class="fad fa-plus-circle text-secondary ml-1"></i></button>
                                </div>
                            </div>
                            <hr>
                            <ul class="lista-detalhes row mx-n1">
                                <li class="col-6 col-sm-4 col-md-6 col-xl-3 px-1 d-flex">
                                    <i class="fa fa-fw fa-users mr-1"></i>
                                    <span>5 passageiros</span>
                                </li>
                                <li class="col-6 col-sm-4 col-md-6 col-xl-3 px-1 d-flex">
                                    <i class="fa fa-fw fa-steering-wheel mr-1"></i>
                                    <span>Manual</span>
                                </li>
                                <li class="col-6 col-sm-4 col-md-6 col-xl-3 px-1 d-flex">
                                    <i class="fa fa-fw fa-faucet-drip mr-1"></i>
                                    <span>Pia cozinha</span>
                                </li>
                                <li class="col-6 col-sm-4 col-md-6 col-xl-3 px-1 d-flex">
                                    <i class="fa fa-fw fa-snowflake mr-1"></i>
                                    <span>Ar condicionado</span>
                                </li>  
                                <li class="col-6 col-sm-4 col-md-6 col-xl-3 px-1 d-flex">
                                    <i class="fa fa-fw fa-bed-alt mr-1"></i>
                                    <span>5 camas</span>
                                </li>
                                <li class="col-6 col-sm-4 col-md-6 col-xl-3 px-1 d-flex">
                                    <i class="far fa-fw fa-th-large mr-1"></i>
                                    <span>Cooktop</span>
                                </li>
                                <li class="col-6 col-sm-4 col-md-6 col-xl-3 px-1 d-flex">
                                    <i class="fa fa-fw fa-refrigerator mr-1"></i>
                                    <span>Refrigerador</span>
                                </li>
                                <li class="col-6 col-sm-4 col-md-6 col-xl-3 px-1 d-flex">
                                    <i class="fa fa-fw fa-shower mr-1"></i>
                                    <span>Banheiro</span>
                                </li>   
                            </ul>
                        </div>
                        <div class="col-md-4 col-xl-3 pr-md-0">
                            <div class="bg-primary rounded box-shadow text-secondary text-center p-3 mb-2 mt-3 my-md-0 mr-md-n4 position-relative">
                                <h4 class="text-white title mb-0">R$450,00</h4>
                                <p>Incluso taxas e impostos</p>
                                <hr class="my-3">
                                <p>Diária: R$75,00</p>
                                <p>7 dias: R$450,00</p>
                                <a href="motorhome.php" class="btn btn-center-bottom btn-secondary">Ver mais</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <!-- Modal especificações -->
    <div class="modal fade" id="modal-espec" tabindex="-1" role="dialog" aria-labelledby="modal-espec-title" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="title mb-0" id="modal-espec-title">AS50 Campervan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="far fa-times text-primary"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Earum eligendi ab quis ullam. Soluta praesentium iusto, dolore quidem aliquam facere aliquid quae tempore dolorum, temporibus, ad aspernatur aperiam dolor mollitia? ipsum, dolor sit amet consectetur adipisicing elit. Explicabo, laborum. Praesentium nostrum soluta nesciunt odit laudantium, nihil ipsa maxime sapiente dolore corrupti voluptate minima omnis quos reiciendis corporis modi. Officiis. ipsum, dolor sit amet consectetur adipisicing elit. Nesciunt culpa quod numquam reiciendis! Deserunt non aliquam dolores saepe rem veritatis molestiae consectetur, quaerat error eum omnis, beatae repellendus unde numquam!</p>
                    <hr>
                    <ul class="lista-detalhes row mx-n1">
                        <li class="col-6 col-sm-4 col-md-6 col-lg-3 px-1 d-flex">
                            <i class="fa fa-fw fa-users mr-1"></i>
                            <span>5 passageiros</span>
                        </li>
                        <li class="col-6 col-sm-4 col-md-6 col-lg-3 px-1 d-flex">
                            <i class="fa fa-fw fa-steering-wheel mr-1"></i>
                            <span>Manual</span>
                        </li>
                        <li class="col-6 col-sm-4 col-md-6 col-lg-3 px-1 d-flex">
                            <i class="fa fa-fw fa-faucet-drip mr-1"></i>
                            <span>Pia cozinha</span>
                        </li>
                        <li class="col-6 col-sm-4 col-md-6 col-lg-3 px-1 d-flex">
                            <i class="fa fa-fw fa-snowflake mr-1"></i>
                            <span>Ar condicionado</span>
                        </li>  
                        <li class="col-6 col-sm-4 col-md-6 col-lg-3 px-1 d-flex">
                            <i class="fa fa-fw fa-bed-alt mr-1"></i>
                            <span>5 camas</span>
                        </li>
                        <li class="col-6 col-sm-4 col-md-6 col-lg-3 px-1 d-flex">
                            <i class="far fa-fw fa-th-large mr-1"></i>
                            <span>Cooktop</span>
                        </li>
                        <li class="col-6 col-sm-4 col-md-6 col-lg-3 px-1 d-flex">
                            <i class="fa fa-fw fa-refrigerator mr-1"></i>
                            <span>Refrigerador</span>
                        </li>
                        <li class="col-6 col-sm-4 col-md-6 col-lg-3 px-1 d-flex">
                            <i class="fa fa-fw fa-shower mr-1"></i>
                            <span>Banheiro</span>
                        </li>  
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <?php include('inc_rodape.php'); ?>
</body>
</html>