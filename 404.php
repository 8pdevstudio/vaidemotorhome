<!doctype html>
<html lang="pt-br">
<head>
    <?php include('inc_head.php'); ?>
</head>
<body>
    <?php include('inc_topo.php'); ?>

    <section class="py-6 py-md-9 sem-contato-rodape">
        <div class="container text-center">
            <img src="assets/images/404.png" alt="Ah... estamos sem rota!" title="Ah... estamos sem rota!" class="img-fluid mx-auto">
            <h1 class="title font-size-xl mt-6 mb-2">Ah... estamos sem rota!</h1>
            <p>Desculpe, mas esta página que você está procurando não está disponível.</p>
        </div>
    </section>

    <?php include('inc_rodape.php'); ?>
</body>
</html>