<!doctype html>
<html lang="pt-br">
<head>
    <?php include('inc_head.php'); ?>
</head>
<body>
    <?php include('inc_topo.php'); ?>

    <section id="banner" class="bg-cover overlay overlay-light pt-8 pt-lg-10 pb-7" style="background-image: url('assets/images/fundo-interna.jpg');">
        <div class="container text-center pt-8 pt-lg-10">
            <h1 class="display-1 d-inline-block text-white mt-6 mb-0 bg-primary px-2 py-1"><span>ROTEIROS ESPECIAIS</span> PARA VIAGENS DE MOTORHOME</h1>
        </div>
    </section>

    <section class="py-7">
        <div class="container">
            <div class="row justify-content-center mb-4">
                <div class="col-md-10 col-lg-6 text-center">
                    <h2 class="title">Os melhores roteiros para você viajar de motorhome!</h2>
                    <p>Preparamos vários roteiros para te inspirar na sua próxima viagem de motorhome. Todos eles são destinos amigáveis, com toda a estrutura disponível para que você aproveite ao máximo sua viagem!</p>
                </div>    
            </div>
            <div class="row mb-4">
                <div class="col-sm-6 col-lg-3">
                    <select class="cs-select secondary">
                        <option value="" disabled selected>País ou região</option>
                        <option value="Opção 1">Opção 1</option>
                        <option value="Opção 2">Opção 2</option>
                        <option value="Opção 3">Opção 3</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <h3 class="title">Destinos disponíveis</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-lg-4 p-2">
                    <a href="destinos-single.php" class="card">
                        <div class="ratio-img ratio-img-16by9">
                            <img src="assets/images/destino.jpg" alt="Grand Canyon" title="Grand Canyon">
                        </div>
                        <div class="card-body d-flex align-items-center justify-content-between">
                            <h3 class="title m-0 col pl-0">Grand Canyon</h3>
                            <span class="btn btn-link btn-arrow col-auto">Ver mais</span>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4 p-2">
                    <a href="destinos-single.php" class="card">
                        <div class="ratio-img ratio-img-16by9">
                            <img src="assets/images/destino.jpg" alt="Grand Canyon" title="Grand Canyon">
                        </div>
                        <div class="card-body d-flex align-items-center justify-content-between">
                            <h3 class="title m-0 col pl-0">Grand Canyon</h3>
                            <span class="btn btn-link btn-arrow col-auto">Ver mais</span>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4 p-2">
                    <a href="destinos-single.php" class="card">
                        <div class="ratio-img ratio-img-16by9">
                            <img src="assets/images/destino.jpg" alt="Grand Canyon" title="Grand Canyon">
                        </div>
                        <div class="card-body d-flex align-items-center justify-content-between">
                            <h3 class="title m-0 col pl-0">Grand Canyon</h3>
                            <span class="btn btn-link btn-arrow col-auto">Ver mais</span>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4 p-2">
                    <a href="destinos-single.php" class="card">
                        <div class="ratio-img ratio-img-16by9">
                            <img src="assets/images/destino.jpg" alt="Grand Canyon" title="Grand Canyon">
                        </div>
                        <div class="card-body d-flex align-items-center justify-content-between">
                            <h3 class="title m-0 col pl-0">Grand Canyon</h3>
                            <span class="btn btn-link btn-arrow col-auto">Ver mais</span>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4 p-2">
                    <a href="destinos-single.php" class="card">
                        <div class="ratio-img ratio-img-16by9">
                            <img src="assets/images/destino.jpg" alt="Grand Canyon" title="Grand Canyon">
                        </div>
                        <div class="card-body d-flex align-items-center justify-content-between">
                            <h3 class="title m-0 col pl-0">Grand Canyon</h3>
                            <span class="btn btn-link btn-arrow col-auto">Ver mais</span>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4 p-2">
                    <a href="destinos-single.php" class="card">
                        <div class="ratio-img ratio-img-16by9">
                            <img src="assets/images/destino.jpg" alt="Grand Canyon" title="Grand Canyon">
                        </div>
                        <div class="card-body d-flex align-items-center justify-content-between">
                            <h3 class="title m-0 col pl-0">Grand Canyon</h3>
                            <span class="btn btn-link btn-arrow col-auto">Ver mais</span>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <?php include('inc_rodape.php'); ?>
</body>
</html>