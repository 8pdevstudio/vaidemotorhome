<!doctype html>
<html lang="pt-br">
<head>
    <?php include('inc_head.php'); ?>
</head>
<body>
    <?php include('inc_topo.php'); ?>

    <section id="banner" class="bg-cover overlay overlay-light pt-8 pt-lg-10 pb-7" style="background-image: url('assets/images/fundo-interna.jpg');">
        <div class="container text-center pt-8 pt-lg-10">
            <h1 class="display-1 d-inline-block text-white mt-6 mb-0 bg-primary px-2 py-1">Quem <span>somos</span></h1>
        </div>
    </section>

    <section class="py-6 py-md-7 bg-secondary">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-10 col-lg-8 col-xl-6 text-center">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vitae nisl bibendum, sollicitudin quam nec, rhoncus lacus. Etiam et sodales mauris. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam vehicula, arcu id rhoncus sagittis, justo metus ultrices lorem, in suscipit dui odio fermentum lectus. Donec vitae dapibus risus, et cursus quam. Nullam in pulvinar velit, ultricies semper est. Nam scelerisque neque ut elit interdum, at condimentum leo ultricies. Aenean elementum mollis velit, id tempor diam. Vivamus auctor arcu a felis condimentum porta sit amet facilisis sem. Aenean tempus metus in facilisis porttitor. Suspendisse ut vestibulum sapien. Cras in neque erat. Morbi semper euismod pharetra.</p>
                </div>
            </div>
        </div>
    </section>

    <section class="py-6 py-md-9">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-9">
                    <div class="d-flex flex-wrap align-items-center justify-content-center">
                        <div class="col-auto">
                            <div class="border-lg rounded-circle p-1 fz-0">
                                <div class="ratio-img ratio-img-avatar-xl box-shadow">
                                    <img src="assets/images/destino.jpg" alt="Mirella Rabelo" title="Mirella Rabelo">
                                </div>
                            </div>
                        </div>
                        <div class="col-md mt-2 mt-md-0 text-center text-md-left">
                            <h3 class="title">Mirella Rabelo</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vitae nisl bibendum, sollicitudin quam nec, rhoncus lacus. Etiam et sodales mauris. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam vehicula, arcu id rhoncus sagittis, justo metus ultrices lorem, in suscipit dui odio fermentum lectus. Donec vitae dapibus risus, et cursus quam. Nullam in pulvinar velit, ultricies semper est.</p>
                        </div>
                    </div>
                    <div class="d-flex flex-wrap align-items-center justify-content-center mt-8">
                        <div class="col-auto">
                            <div class="border-lg rounded-circle p-1 fz-0">
                                <div class="ratio-img ratio-img-avatar-xl box-shadow">
                                    <img src="assets/images/destino.jpg" alt="Rômulo Wolf" title="Rômulo Wolf">
                                </div>
                            </div>
                        </div>
                        <div class="col-md mt-2 mt-md-0 text-center text-md-left">
                            <h3 class="title">Rômulo Wolf</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vitae nisl bibendum, sollicitudin quam nec, rhoncus lacus. Etiam et sodales mauris. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam vehicula, arcu id rhoncus sagittis, justo metus ultrices lorem, in suscipit dui odio fermentum lectus. Donec vitae dapibus risus, et cursus quam. Nullam in pulvinar velit, ultricies semper est.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="vantagens">
        <div class="container bg-cover py-7 rounded box-shadow text-center" style="background-image: url('assets/images/bg-vantagens.jpg');">
            <div class="row justify-content-center mb-2">
                <div class="col-md-6">
                    <h3 class="title">Porque viajar com o Vai de Motorhome?</h3>
                </div>
            </div>
            <div class="row justify-content-center mx-0 scrollable-sm">
                <div class="col-md-6 col-lg-4 py-3 ml-0">
                    <div class="border-lg border-width-2 p-1 fz-0 d-inline-block rounded-circle">
                        <div class="ratio-img ratio-img-avatar-lg bg-white rounded-circle">
                            <i class="fal fa-user-headset"></i>
                        </div>
                    </div>
                    <strong class="d-block mt-2 mb-1 font-size-lg">Atendimento personalizado</strong>
                    <p>Sabemos que viajar de motorhome é novidade para muita gente. Por isso temos uma equipe a disposição para esclarecer qualquer dúvida.</p>
                </div>
                <div class="col-md-6 col-lg-4 py-3 ml-0">
                    <div class="border-lg border-width-2 p-1 fz-0 d-inline-block rounded-circle">
                        <div class="ratio-img ratio-img-avatar-lg bg-white rounded-circle">
                            <i>R$</i>
                        </div>
                    </div>
                    <strong class="d-block mt-2 mb-1 font-size-lg">Pagamento em Reais</strong>
                    <p>O Pagamento é em reais. Assim você não corre o risco de sofrer com alterações cambiais.</p>
                </div>
                <div class="col-md-6 col-lg-4 py-3 ml-0">
                    <div class="border-lg border-width-2 p-1 fz-0 d-inline-block rounded-circle">
                        <div class="ratio-img ratio-img-avatar-lg bg-white rounded-circle">
                            <i class="fal fa-rv"></i>
                        </div>
                    </div>
                    <strong class="d-block mt-2 mb-1 font-size-lg">Seleção dos melhores Motorhomes</strong>
                    <p>Selecionamos como frota somente motorhomes que já usamos e aprovamos.</p>
                </div>
                <div class="col-md-6 col-lg-4 py-3 ml-0">
                    <div class="border-lg border-width-2 p-1 fz-0 d-inline-block rounded-circle">
                        <div class="ratio-img ratio-img-avatar-lg bg-white rounded-circle">
                            <i class="fal fa-users"></i>
                        </div>
                    </div>
                    <strong class="d-block mt-2 mb-1 font-size-lg">Feito por quem entende de  viagens de Motorhome</strong>
                    <p>Nossos fundadores já viajaram em inúmeros modelos de motorhome, tanto que escolheram um para viver em tempo integral.</p>
                </div>
                <div class="col-md-6 col-lg-4 py-3 ml-0">
                    <div class="border-lg border-width-2 p-1 fz-0 d-inline-block rounded-circle">
                        <div class="ratio-img ratio-img-avatar-lg bg-white rounded-circle">
                            <i class="fal fa-language"></i>
                        </div>
                    </div>
                    <strong class="d-block mt-2 mb-1 font-size-lg">Instruções de retirada em português</strong>
                    <p>Todas as Instruções para retirada do motorhome em Português.</p>
                </div>
            </div>
        </div>
    </section>

    <section id="depoimentos" class="pt-7 z-1 mb-n7 overflow-hidden">
        <div class="container position-relative">
            <div class="row justify-content-center">
                <div class="col-md-10 text-center">
                    <h4 class="title mb-4">Quem viajou recomenda</h4>
                    <div class="slick-depoimentos">
                        <div class="d-flex align-items-stretch flex-column flex-lg-row bg-primary text-secondary rounded box-shadow overflow-hidden text-left">
                            <div class="col-lg-6 bg-cover py-9 py-lg-0" style="background-image: url('assets/images/depoimento.jpg');"></div>
                            <div class="col-lg-6 pt-6 px-4 pb-4 p-lg-6">
                                <div class="font-italic mb-3">
                                    <p>“Lorem ipsum dolor sit, amet consectetur adipisicing elit. Inventore, laborum quis distinctio obcaecati voluptates vero dolorem pariatur quos iusto fugit maiores voluptas nobis maxime ab est at ducimus necessitatibus! Similique. ipsum dolor sit amet consectetur adipisicing elit. Quo harum doloribus, fugiat nam magnam impedit explicabo quod cumque, numquam soluta nemo voluptates laboriosam molestiae, dignissimos voluptas repudiandae maxime et atque.”</p>
                                </div>
                                <strong>Renata Ramos, grupo de viagens Europa 2019</strong>
                            </div>
                        </div>
                        <div class="d-flex align-items-stretch flex-column flex-lg-row bg-primary text-secondary rounded box-shadow overflow-hidden text-left">
                            <div class="col-lg-6 bg-cover py-9 py-lg-0" style="background-image: url('assets/images/depoimento.jpg');"></div>
                            <div class="col-lg-6 pt-6 px-4 pb-4 p-lg-6">
                                <div class="font-italic mb-3">
                                    <p>“Lorem ipsum dolor sit, amet consectetur adipisicing elit. Inventore, laborum quis distinctio obcaecati voluptates vero dolorem pariatur quos iusto fugit maiores voluptas nobis maxime ab est at ducimus necessitatibus! Similique. ipsum dolor sit amet consectetur adipisicing elit. Quo harum doloribus, fugiat nam magnam impedit explicabo quod cumque, numquam soluta nemo voluptates laboriosam molestiae, dignissimos voluptas repudiandae maxime et atque.”</p>
                                </div>
                                <strong>Renata Ramos, grupo de viagens Europa 2019</strong>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php include('inc_rodape.php'); ?>
</body>
</html>