<!doctype html>
<html lang="pt-br">
<head>
    <?php include('inc_head.php'); ?>
</head>
<body>
    <?php include('inc_topo.php'); ?>

    <section id="banner" class="pb-6 pb-md-8 overflow-hidden">
        <div class="bg-cover overlay overlay-light pt-4 pt-md-8 pt-lg-10 pb-9" style="background-image: url('assets/images/fundo-interna.jpg');">
            <div class="container text-center pt-8 pt-lg-10">
                <h1 class="display-1 d-inline-block text-white mt-6 mb-0 bg-primary px-2 py-1">Alugue agora <span>seu motorhome</span></h1>
            </div>
        </div>
        <div class="bg-secondary pb-sm-9">
            <div class="container">
                <form class="mx-lg-n6 mx-xl-n4 pb-6 pb-md-2 p-2 bg-primary text-white box-shadow rounded position-relative z-2 t-n7">
                    <div class="dropdown dropdown-static d-inline-block">
                        <a class="py-1 text-white dropdown-toggle" href="#" id="dropdown-passageiros" data-toggle="dropdown"><span id="qtd-passageiros">0</span> passageiro(s)</a>
                        <div class="dropdown-menu" aria-labelledby="dropdown-passageiros">
                            <div class="d-flex align-items-center justify-content-between p-2 text-nowrap">
                                <span>Adulto (18+)</span>
                                <div class="input-group input-number ml-4">
                                    <div class="input-group-prepend">
                                        <button class="btn btn-minus" type="button"><i class="far fa-minus"></i></button>
                                    </div>
                                    <input type="number" class="form-control" value="0" min="0" step="1" data-increment="qtd-passageiros" readonly>
                                    <div class="input-group-append">
                                        <button class="btn btn-plus" type="button"><i class="far fa-plus"></i></button>
                                    </div>
                                </div>
                            </div>
                            <div class="dropdown-divider my-0"></div>
                            <div class="d-flex align-items-center justify-content-between p-2 text-nowrap">
                                <span>Jovem (0-17)</span>
                                <div class="input-group input-number ml-4">
                                    <div class="input-group-prepend">
                                        <button class="btn text-primary btn-minus" type="button"><i class="far fa-minus"></i></button>
                                    </div>
                                    <input type="number" class="form-control" value="0" min="0" step="1" data-increment="qtd-passageiros" readonly>
                                    <div class="input-group-append">
                                        <button class="btn text-primary btn-plus" type="button"><i class="far fa-plus"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-1 mb-n1 mx-n1 z-1 position-relative">
                        <div class="col-sm-12 col-md-4 col-lg-2 p-1">
                            <select class="cs-select cs-select--wrap maw-100">
                                <option value="" disabled selected>País</option>
                                <option value="opcao1">Opção 1Opção 1Opção 1Opção 1Opção 1Opção 1Opção 1</option>
                                <option value="opcao2">Opção 1</option>
                                <option value="opcao2">Opção 2</option>
                                <option value="opcao3">Opção 3</option>
                            </select>
                        </div>
                        <div class="col-sm-6 col-md-4 col-lg-2 p-1">
                            <select class="cs-select cs-select--wrap maw-100">
                                <option value="" disabled selected>Retirar em</option>
                                <option value="opcao1">Opção 1Opção 1Opção 1Opção 1Opção 1Opção 1Opção 1</option>
                                <option value="opcao1">Opção 1</option>
                                <option value="opcao2">Opção 2</option>
                                <option value="opcao3">Opção 3</option>
                            </select>
                        </div>
                        <div class="col-sm-6 col-md-4 col-lg-2 p-1">
                            <select class="cs-select cs-select--wrap maw-100">
                                <option value="" disabled selected>Entregar em</option>
                                <option value="opcao1">Opção 1Opção 1Opção 1Opção 1Opção 1Opção 1Opção 1</option>
                                <option value="opcao1">Opção 1</option>
                                <option value="opcao2">Opção 2</option>
                                <option value="opcao3">Opção 3</option>
                            </select>
                        </div>
                        <div class="col-sm-6 col-md-4 col-lg-2 p-1">
                            <div class="form-group">
                                <label class="label-abs">Data de retirada</label>
                                <input type="date" class="form-control" placeholder="Data da retirada" />
                                <i class="far fa-calendar-alt form-icon"></i>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4 col-lg-2 p-1">
                            <div class="form-group">
                                <label class="label-abs">Data de entrega</label>
                                <input type="date" class="form-control" placeholder="Data da entrega" />
                                <i class="far fa-calendar-alt form-icon"></i>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4 col-lg-2 p-1 d-none d-md-block">
                            <button type="submit" class="btn btn-block btn-secondary">Buscar</button>
                        </div>
                    </div>
                    <div class="row mx-n1 mx-md-n2 mb-n1 mt-1 align-items-center">
                        <div class="col-md-auto p-1 px-md-2">
                            <div class="custom-control custom-switch">
                                <input type="checkbox" class="custom-control-input" id="possui-hab-int">
                                <label class="custom-control-label" for="possui-hab-int">Possui habilitação internacional</label>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-auto p-1 px-md-2">
                            <select class="cs-select cs-select--transparent cs-select--wrap">
                                <option value="" disabled selected>País da licença para dirigir</option>
                                <option value="opcao1">Opção 1Opção 1Opção 1Opção 1Opção 1Opção 1Opção 1</option>
                                <option value="opcao1">Opção 1</option>
                                <option value="opcao2">Opção 2</option>
                                <option value="opcao3">Opção 3</option>
                            </select>
                        </div>
                        <div class="col-sm-6 col-md-auto p-1 px-md-2">
                            <div class="dropdown dropdown-static d-none d-md-block">
                                <a class="py-1 text-white small dropdown-toggle" href="#" id="dropdown-idade" data-toggle="dropdown">Idade</a>
                                <div class="dropdown-menu p-2" aria-labelledby="dropdown-idade">
                                    <div class="input-group input-number">
                                        <div class="input-group-prepend">
                                            <button class="btn text-primary btn-minus" type="button"><i class="far fa-minus"></i></button>
                                        </div>
                                        <input type="number" class="form-control" value="18" min="18" step="1" readonly>
                                        <div class="input-group-append">
                                            <button class="btn text-primary btn-plus" type="button"><i class="far fa-plus"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group d-md-none">
                                <input type="text" class="form-control bg-white" value="Idade" readonly />
                                <div class="form-icon">
                                    <div class="input-group input-number input-number-sm">
                                        <div class="input-group-prepend">
                                            <button class="btn text-primary btn-minus" type="button"><i class="far fa-minus"></i></button>
                                        </div>
                                        <input type="number" class="form-control" value="18" min="18" step="1" readonly>
                                        <div class="input-group-append">
                                            <button class="btn text-primary btn-plus" type="button"><i class="far fa-plus"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-center-bottom btn-secondary d-md-none">Buscar</button>
                </form>
                <h4 class="title text-center d-none d-sm-block">Com 3 passos simples você começa a viajar!</h4>
            </div>
        </div>
        <div class="passos pt-4 pt-sm-0">
            <div class="container">
                <h4 class="title text-center d-sm-none">Com 3 passos simples você começa a viajar!</h4>
                <div class="row justify-content-center scrollable-xs scrollable-2 pt-2 pt-sm-0 mt-2 mt-sm-0">
                    <div class="col-sm-4 col-md-3 col-xl-2 ml-0">
                        <div class="passo passo-1">
                            <div class="img">
                                <div class="ratio-img ratio-img-1by1 rounded-circle bg-secondary">
                                    <i class="fad fa-route"></i>
                                </div>
                            </div>
                            <h4 class="title mt-2">Lorem ipsum dolor sit amet</h4>
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-3 col-xl-2 ml-0">
                        <div class="passo passo-2">
                            <div class="img">
                                <div class="ratio-img ratio-img-1by1 rounded-circle bg-secondary">
                                    <img src="assets/images/destino.jpg" alt="Passo 1" title="Passo 1">
                                </div>
                            </div>
                            <h4 class="title mt-2">Lorem ipsum dolor sit amet</h4>
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-3 col-xl-2 ml-0">
                        <div class="passo passo-3">
                            <div class="img">
                                <div class="ratio-img ratio-img-1by1 rounded-circle bg-secondary">
                                    <i class="fad fa-rv"></i>
                                </div>
                            </div>
                            <h4 class="title mt-2">Lorem ipsum dolor sit amet</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="destinos" class="py-6 py-md-9 bg-primary text-white bg-cover" style="background-image: url('assets/images/bg-destinos.jpg');">
        <div class="container">
            <h2 class="display-3 mb-6 text-center">Destinos <span>mais visitados</span></h2>
            <div class="row scrollable-sm scrollbar-secondary">
                <div class="col-md-6 col-lg-4 p-2 ml-0">
                    <a href="destinos-single.php" class="card">
                        <div class="ratio-img ratio-img-16by9">
                            <img src="assets/images/destino.jpg" alt="Grand Canyon" title="Grand Canyon">
                        </div>
                        <div class="card-body d-flex align-items-center justify-content-between">
                            <h3 class="title m-0 col pl-0">Grand Canyon</h3>
                            <span class="btn btn-link btn-arrow col-auto">Ver mais</span>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4 p-2 ml-0">
                    <a href="destinos-single.php" class="card">
                        <div class="ratio-img ratio-img-16by9">
                            <img src="assets/images/destino.jpg" alt="Grand Canyon" title="Grand Canyon">
                        </div>
                        <div class="card-body d-flex align-items-center justify-content-between">
                            <h3 class="title m-0 col pl-0">Grand Canyon</h3>
                            <span class="btn btn-link btn-arrow col-auto">Ver mais</span>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4 p-2 ml-0">
                    <a href="destinos-single.php" class="card">
                        <div class="ratio-img ratio-img-16by9">
                            <img src="assets/images/destino.jpg" alt="Grand Canyon" title="Grand Canyon">
                        </div>
                        <div class="card-body d-flex align-items-center justify-content-between">
                            <h3 class="title m-0 col pl-0">Grand Canyon</h3>
                            <span class="btn btn-link btn-arrow col-auto">Ver mais</span>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4 p-2 ml-0">
                    <a href="destinos-single.php" class="card">
                        <div class="ratio-img ratio-img-16by9">
                            <img src="assets/images/destino.jpg" alt="Grand Canyon" title="Grand Canyon">
                        </div>
                        <div class="card-body d-flex align-items-center justify-content-between">
                            <h3 class="title m-0 col pl-0">Grand Canyon</h3>
                            <span class="btn btn-link btn-arrow col-auto">Ver mais</span>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4 p-2 ml-0">
                    <a href="destinos-single.php" class="card">
                        <div class="ratio-img ratio-img-16by9">
                            <img src="assets/images/destino.jpg" alt="Grand Canyon" title="Grand Canyon">
                        </div>
                        <div class="card-body d-flex align-items-center justify-content-between">
                            <h3 class="title m-0 col pl-0">Grand Canyon</h3>
                            <span class="btn btn-link btn-arrow col-auto">Ver mais</span>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4 p-2 ml-0">
                    <a href="destinos-single.php" class="card">
                        <div class="ratio-img ratio-img-16by9">
                            <img src="assets/images/destino.jpg" alt="Grand Canyon" title="Grand Canyon">
                        </div>
                        <div class="card-body d-flex align-items-center justify-content-between">
                            <h3 class="title m-0 col pl-0">Grand Canyon</h3>
                            <span class="btn btn-link btn-arrow col-auto">Ver mais</span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="row justify-content-center mt-6">
                <div class="col-md-6 col-lg-4">
                    <a href="destinos.php" class="btn btn-block btn-arrow btn-secondary">Veja mais destinos</a>
                </div>
            </div>
        </div>
    </section>

    <section id="vantagens" class="pt-6 pb-8 py-md-9">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <img src="assets/images/vantagens.jpg" alt="Por que viajar com a Vai de Motorhome?" title="Por que viajar com a Vai de Motorhome?" class="img-fluid">
                </div>
                <div class="col-md-6 col-lg-5 offset-lg-1 mt-4 mt-md-0">
                    <div class="bg-primary rounded box-shadow-lg p-6 text-white">
                        <h4 class="text-secondary title">Por que viajar com a Vai de Motorhome?</h4>
                        <div class="texto-formatado mb-4">
                            <ul>
                                <li>Atendimento <b>personalizado</b></li>
                                <li>Pagamento em <b>reais</b></li>
                                <li>Motorhomes <b>selecionados</b></li>
                                <li><b>Feito por quem entende</b> de motorhome</li>
                                <li>Instruções de retirada em <b>português</b></li>
                            </ul>
                        </div>
                        <a href="quem-somos.php#vantagens" class="btn btn-arrow btn-center-bottom btn-secondary">Saiba mais</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="ratio-img ratio-img-16by9 d-lg-none">
        <img src="assets/images/quem-somos.jpg" alt="Feito por quem entende de Motorhome" title="Feito por quem entende de Motorhome">
    </div>
    <section id="quem-somos" class="bg-primary position-relative">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 pt-6 pb-7 py-md-6 px-2 py-lg-9 px-lg-7">
                    <div class="bg-secondary rounded box-shadow-lg text-center p-6 mb-3 position-relative">
                        <h4 class="title">Feito por quem entende de Motorhome</h4>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Expedita dolores eaque officia explicabo nihil, ipsa deleniti nisi corporis voluptatem quidem, accusamus deserunt exercitationem dolorem, labore aut delectus est laudantium. Totam. ipsum dolor sit amet consectetur, adipisicing elit. Magnam in quos iusto corrupti odit modi necessitatibus, natus qui atque possimus aut sint alias quae, nihil reiciendis laborum doloribus! Voluptatum, tempore?</p>
                        <a href="quem-somos.php" class="btn btn-arrow btn-center-bottom btn-primary">Sobre nós</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 bg-cover position-absolute t-0 r-0 b-0 d-none d-lg-block" style="background-image: url('assets/images/quem-somos.jpg');"></div>
    </section>

    <section id="viagens-em-grupo" class="py-6 py-lg-9">
        <div class="container position-relative">
            <div class="row justify-content-center mb-4">
                <div class="col-md-8 col-lg-6 text-center">
                    <h4 class="title">Viagens em grupo</h4>
                    <p>Conheça nossos circuitos pela Europa e Estados Unidos! Experiências exclusivas, guias experientes e assistência personalizada. Imersão completa e novas amizades!</p>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="ratio-img ratio-img-16by9 rounded-lg box-shadow">
                        <img src="assets/images/viagens-em-grupo.jpg" alt="Viagens em grupo" title="Viagens em grupo" class="img-fluid">
                    </div>
                </div>
            </div>
            <a href="viagens-em-grupo.php" class="btn btn-arrow btn-center-bottom btn-secondary">Conheça nossos grupos</a>
        </div>
    </section>

    <section id="depoimentos" class="pt-6 z-1 mb-n7 overflow-hidden">
        <div class="container position-relative">
            <div class="row justify-content-center">
                <div class="col-xl-10 text-center">
                    <h4 class="title mb-4">Quem viajou recomenda</h4>
                    <div class="slick-depoimentos">
                        <div class="d-flex align-items-stretch flex-column flex-lg-row bg-primary text-secondary rounded box-shadow overflow-hidden text-left">
                            <div class="col-lg-6 bg-cover py-9 py-lg-0" style="background-image: url('assets/images/depoimento.jpg');"></div>
                            <div class="col-lg-6 pt-6 px-4 pb-4 p-lg-6">
                                <div class="font-italic mb-3">
                                    <p>“Lorem ipsum dolor sit, amet consectetur adipisicing elit. Inventore, laborum quis distinctio obcaecati voluptates vero dolorem pariatur quos iusto fugit maiores voluptas nobis maxime ab est at ducimus necessitatibus! Similique. ipsum dolor sit amet consectetur adipisicing elit. Quo harum doloribus, fugiat nam magnam impedit explicabo quod cumque, numquam soluta nemo voluptates laboriosam molestiae, dignissimos voluptas repudiandae maxime et atque.”</p>
                                </div>
                                <strong>Renata Ramos, grupo de viagens Europa 2019</strong>
                            </div>
                        </div>
                        <div class="d-flex align-items-stretch flex-column flex-lg-row bg-primary text-secondary rounded box-shadow overflow-hidden text-left">
                            <div class="col-lg-6 bg-cover py-9 py-lg-0" style="background-image: url('assets/images/depoimento.jpg');"></div>
                            <div class="col-lg-6 pt-6 px-4 pb-4 p-lg-6">
                                <div class="font-italic mb-3">
                                    <p>“Lorem ipsum dolor sit, amet consectetur adipisicing elit. Inventore, laborum quis distinctio obcaecati voluptates vero dolorem pariatur quos iusto fugit maiores voluptas nobis maxime ab est at ducimus necessitatibus! Similique. ipsum dolor sit amet consectetur adipisicing elit. Quo harum doloribus, fugiat nam magnam impedit explicabo quod cumque, numquam soluta nemo voluptates laboriosam molestiae, dignissimos voluptas repudiandae maxime et atque.”</p>
                                </div>
                                <strong>Renata Ramos, grupo de viagens Europa 2019</strong>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php include('inc_rodape.php'); ?>
</body>
</html>