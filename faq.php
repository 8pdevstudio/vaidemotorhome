<!doctype html>
<html lang="pt-br">
<head>
    <?php include('inc_head.php'); ?>
</head>
<body>
    <?php include('inc_topo.php'); ?>

    <main class="py-4 py-lg-8 sem-contato-rodape">
        <div class="container">
            <div class="row align-items-stretch">
                <div class="col-lg-8">
                    <h1 class="title mb-3">Dúvidas Frequentes</h1>
                    <div class="border border-light rounded p-2 mt-2">
                        <a class="btn btn-link btn-faq collapsed" data-toggle="collapse" href="#faq-1" role="button" aria-expanded="false" aria-controls="faq-1">
                            <i class="fa fa-minus rounded-icon mr-2"></i>Link with href
                        </a>
                        <div class="collapse" id="faq-1">
                            <div class="pt-3 pl-6">
                                <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.</p>
                            </div>
                        </div>
                    </div>
                    <div class="border border-light rounded p-2 mt-2">
                        <a class="btn btn-link btn-faq collapsed" data-toggle="collapse" href="#faq-2" role="button" aria-expanded="false" aria-controls="faq-2">
                            <i class="fa fa-minus rounded-icon mr-2"></i>Link with href
                        </a>
                        <div class="collapse" id="faq-2">
                            <div class="pt-3 pl-6">
                                <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.</p>
                            </div>
                        </div>
                    </div>
                    <div class="border border-light rounded p-2 mt-2">
                        <a class="btn btn-link btn-faq collapsed" data-toggle="collapse" href="#faq-3" role="button" aria-expanded="false" aria-controls="faq-3">
                            <i class="fa fa-minus rounded-icon mr-2"></i>Link with href
                        </a>
                        <div class="collapse" id="faq-3">
                            <div class="pt-3 pl-6">
                                <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.</p>
                            </div>
                        </div>
                    </div>
                    <div class="border border-light rounded p-2 mt-2">
                        <a class="btn btn-link btn-faq collapsed" data-toggle="collapse" href="#faq-4" role="button" aria-expanded="false" aria-controls="faq-4">
                            <i class="fa fa-minus rounded-icon mr-2"></i>Link with href
                        </a>
                        <div class="collapse" id="faq-4">
                            <div class="pt-3 pl-6">
                                <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.</p>
                            </div>
                        </div>
                    </div>
                    <div class="border border-light rounded p-2 mt-2">
                        <a class="btn btn-link btn-faq collapsed" data-toggle="collapse" href="#faq-5" role="button" aria-expanded="false" aria-controls="faq-5">
                            <i class="fa fa-minus rounded-icon mr-2"></i>Link with href
                        </a>
                        <div class="collapse" id="faq-5">
                            <div class="pt-3 pl-6">
                                <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.</p>
                            </div>
                        </div>
                    </div>
                    <div class="border border-light rounded p-2 mt-2">
                        <a class="btn btn-link btn-faq collapsed" data-toggle="collapse" href="#faq-6" role="button" aria-expanded="false" aria-controls="faq-6">
                            <i class="fa fa-minus rounded-icon mr-2"></i>Link with href
                        </a>
                        <div class="collapse" id="faq-6">
                            <div class="pt-3 pl-6">
                                <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <form class="col-lg-4 pl-xl-6 mt-6 mt-lg-0">
                    <div class="bg-primary text-white box-shadow rounded p-3 position-relative">
                        <div class="form-group">
                            <select class="cs-select cs-select--wrap maw-100">
                                <option value="" disabled selected>País</option>
                                <option value="opcao1">Opção 1Opção 1Opção 1Opção 1Opção 1Opção 1Opção 1</option>
                                <option value="opcao2">Opção 1</option>
                                <option value="opcao2">Opção 2</option>
                                <option value="opcao3">Opção 3</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <select class="cs-select cs-select--wrap maw-100">
                                <option value="" disabled selected>Retirar em</option>
                                <option value="opcao1">Opção 1Opção 1Opção 1Opção 1Opção 1Opção 1Opção 1</option>
                                <option value="opcao1">Opção 1</option>
                                <option value="opcao2">Opção 2</option>
                                <option value="opcao3">Opção 3</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <select class="cs-select cs-select--wrap maw-100">
                                <option value="" disabled selected>Entregar em</option>
                                <option value="opcao1">Opção 1Opção 1Opção 1Opção 1Opção 1Opção 1Opção 1</option>
                                <option value="opcao1">Opção 1</option>
                                <option value="opcao2">Opção 2</option>
                                <option value="opcao3">Opção 3</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="date" class="form-control" placeholder="Data da retirada" />
                            <i class="far fa-calendar-alt form-icon"></i>
                        </div>
                        <div class="form-group">
                            <input type="date" class="form-control" placeholder="Data da entrega" />
                            <i class="far fa-calendar-alt form-icon"></i>
                        </div>
                        <div class="form-group">
                            <div class="custom-control custom-switch">
                                <input type="checkbox" class="custom-control-input" id="possui-hab-int">
                                <label class="custom-control-label" for="possui-hab-int">Possui habilitação internacional</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <select class="cs-select cs-select--wrap maw-100">
                                <option value="" disabled selected>País da licença para dirigir</option>
                                <option value="opcao1">Opção 1Opção 1Opção 1Opção 1Opção 1Opção 1Opção 1</option>
                                <option value="opcao1">Opção 1</option>
                                <option value="opcao2">Opção 2</option>
                                <option value="opcao3">Opção 3</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control bg-white" value="Idade" readonly />
                            <div class="form-icon">
                                <div class="input-group input-number input-number-sm">
                                    <div class="input-group-prepend">
                                        <button class="btn text-primary btn-minus" type="button"><i class="far fa-minus"></i></button>
                                    </div>
                                    <input type="number" class="form-control" value="18" min="18" step="1" readonly>
                                    <div class="input-group-append">
                                        <button class="btn text-primary btn-plus" type="button"><i class="far fa-plus"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-secondary btn-center-bottom" type="submit">Buscar</button>
                    </div>
                    <div class="text-center mt-8">
                        <h4 class="title mb-4">Ficou alguma dúvida? <br>Fale conosco pelo chat!</h4>
                        <a href="contato.php" class="btn btn-big-icon btn-primary">Entrar em contato</a>
                    </div>
                </form>
            </div>
        </div>
    </main>

    <?php include('inc_rodape.php'); ?>
</body>
</html>