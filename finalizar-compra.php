<!doctype html>
<html lang="pt-br">
<head>
    <?php include('inc_head.php'); ?>
</head>
<body>
    <?php include('inc_topo.php'); ?>

    <section class="py-4 py-lg-7 sem-contato-rodape">
        <div class="container">
            <h2 class="title mb-2 mb-md-4">Resumo dos seus pedidos</h2>
            <div class="item-motorhome py-4 pr-2 pr-lg-0 mb-6 mb-lg-4 mr-lg-2 small text-center text-md-left">
                <div class="col-lg-9 col-xl-10">
                    <div class="row align-items-center">
                        <div class="col-md-6 col-xl-2 mb-2 mb-md-4 mb-xl-0">
                            <div class="ratio-img ratio-img-4by3">
                                <img src="assets/images/motorhome.jpg" alt="AS50 Campervan" title="AS50 Campervan">
                            </div>
                        </div>
                        <div class="col-md-6 col-xl-3 border-right-xl text-xl-center mb-4 mb-xl-0">
                            <span class="d-block">Aluguel de motorhome</span>
                            <h2 class="title mb-2 mb-md-4">AS50 Campervan</h2>
                            <span class="d-block mb-1">Fornecido por:</span>
                            <img src="assets/images/logo.png" alt="Fornecedor" title="Fornecedor" class="img-fluid">
                        </div>
                        <div class="col-md-5 col-xl-3 border-right">
                            <div class="d-flex justify-content-center mb-1">
                                <i class="far fa-fw fa-calendar-alt font-size-base mr-1"></i>
                                <div class="col-auto col-md px-0">
                                    <strong>Data da retirada: </strong>
                                    <span>06/06/2020</span>
                                </div>
                            </div>
                            <div class="d-flex justify-content-center mb-4">
                                <i class="far fa-fw fa-map-marker-alt font-size-base mr-1"></i>
                                <div class="col-auto col-md px-0">
                                    <strong>local da retirada: </strong>
                                    <span>Madri, Espanha - Road Europe Motorhomes</span>
                                </div>
                            </div>
                            <div class="d-flex justify-content-center mb-1">
                                <i class="far fa-fw fa-calendar-alt font-size-base mr-1"></i>
                                <div class="col-auto col-md px-0">
                                    <strong>Data da entrega: </strong>
                                    <span>06/06/2020</span>
                                </div>
                            </div>
                            <div class="d-flex justify-content-center">
                                <i class="far fa-fw fa-map-marker-alt font-size-base mr-1"></i>
                                <div class="col-auto col-md px-0">
                                    <strong>local da entrega: </strong>
                                    <span>Madri, Espanha - Road Europe Motorhomes</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5 col-xl-3 border-right mt-4 mt-md-0">
                            <strong class="font-size-base d-block mb-2">Serviços e extras inclusos:</strong>
                            <ul>
                                <li class="d-flex justify-content-center mt-1">
                                    <i class="fa fa-fw fa-check rounded-icon text-secondary mr-1"></i>
                                    <div class="col-auto col-md px-0 lh-2">
                                        <span>Lorem ipsum sit dalamet</span>
                                    </div>
                                </li>
                                <li class="d-flex justify-content-center mt-1">
                                    <i class="fa fa-fw fa-check rounded-icon text-secondary mr-1"></i>
                                    <div class="col-auto col-md px-0 lh-2">
                                        <span>Lorem ipsum sit dalamet</span>
                                    </div>
                                </li>
                                <li class="d-flex justify-content-center mt-1">
                                    <i class="fa fa-fw fa-check rounded-icon text-secondary mr-1"></i>
                                    <div class="col-auto col-md px-0 lh-2">
                                        <span>Lorem ipsum sit dalamet</span>
                                    </div>
                                </li>
                                <li class="d-flex justify-content-center mt-1">
                                    <i class="fa fa-fw fa-check rounded-icon text-secondary mr-1"></i>
                                    <div class="col-auto col-md px-0 lh-2">
                                        <span>Lorem ipsum sit dalamet</span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-2 col-xl-1 text-center mt-4 mt-md-0">
                            <a href="#" class="d-inline-block p-2 p-md-0"><i class="fa fa-trash-alt d-block font-size-base pb-1"></i>Excluir pedido</a>
                            <br class="d-none d-md-block">
                            <br class="d-none d-md-block">
                            <a href="#" class="d-inline-block p-2 p-md-0"><i class="fa fa-exchange d-block font-size-base pb-1"></i>Trocar pedido</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-xl-2 pr-lg-00">
                    <div class="bg-primary rounded box-shadow text-secondary text-center p-2 mr-lg-n4 mt-2 mt-md-4 mb-n6 my-lg-0 position-relative">
                        <h4 class="text-white title mb-0">R$450,00</h4>
                        <p>Incluso taxas e impostos</p>
                        <hr class="my-2">
                        <p class="mb-1">Diária: R$75,00</p>
                        <p>Viaje até 20 dias</p>
                    </div>
                </div>
            </div>
            <div class="text-center text-md-left">    
                <a href="motorhome.php" class="btn btn-link btn-arrow-left">Voltar para a página de [nome da página]</a>
            </div>
            <form class="row mt-6">
                <div class="col-md-7 col-lg-8 pr-md-0 pr-lg-2">
                    <h3 class="title text-center text-lg-left">Finalize sua compra <span class="d-block d-lg-inline-block mt-2 mb-1 my-lg-0 ml-lg-6 font-weight-normal font-size-base ff-base">Já está registrado?</span><a href="login.php" class="btn btn-secondary ml-lg-2 va-initial">Faça login</a></h3>
                    <strong class="title mt-4 mt-lg-0 d-block"><small>Seus dados</small></strong>
                    <div class="row mx-n1">
                        <div class="col-md-12 col-lg-6 col-xl-4 p-1">
                            <div class="form-group">
                                <input type="text" class="form-control border-light" placeholder="Nome completo" required>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6 col-xl-4 p-1">
                            <div class="form-group">
                                <input type="email" class="form-control border-light" placeholder="E-mail" required>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4 col-xl-4 p-1">
                            <div class="form-group">
                                <label class="label-abs">Data de nascimento</label>
                                <input type="date" class="form-control border-light" placeholder="Nascimento" />
                                <i class="far fa-calendar-alt form-icon text-primary"></i>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-8 col-xl-4 p-1">
                            <div class="form-group">
                                <input type="text" class="form-control border-light" placeholder="Endereço" required>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4 col-xl-3 p-1">
                            <div class="form-group">
                                <input type="text" class="form-control border-light" placeholder="Cidade" required>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4 col-xl-3 p-1">
                            <div class="form-group">
                                <select class="cs-select cs-select--wrap border-light">
                                    <option value="" disabled selected>Estado</option>
                                    <option value="opcao1">São Paulo</option>
                                    <option value="opcao2">Bahia</option>
                                    <option value="opcao2">Minas Gerais</option>
                                    <option value="opcao3">Mato Grosso do Sul</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4 col-xl-2 p-1">
                            <div class="form-group">
                                <input type="tel" class="form-control border-light mask-cep" placeholder="CEP" required>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6 col-xl-3 p-1">
                            <div class="form-group">
                                <input type="tel" class="form-control border-light mask-tel" placeholder="Celular" required>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6 col-xl-3 p-1">
                            <div class="form-group">
                                <input type="tel" class="form-control border-light mask-tel" placeholder="Telefone" required>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6 col-xl-3 p-1">
                            <div class="form-group">
                                <input type="text" class="form-control border-light" placeholder="RG" required>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6 col-xl-3 p-1">
                            <div class="form-group">
                                <input type="text" class="form-control border-light" placeholder="CPF" required>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <strong class="title"><small>Dados de pagamento</small></strong>
                    <div class="row mx-n1 align-items-center">
                        <div class="col-12 p-1">
                            <div class="custom-control custom-radio d-inline-block">
                                <input type="radio" class="custom-control-input" name="cartao" id="cartao-cc">
                                <label class="custom-control-label" for="cartao-cc">Cartão de crédito</label>
                            </div>
                            <div class="custom-control custom-radio d-sm-inline-block mt-2 mt-sm-0 ml-sm-4">
                                <input type="radio" class="custom-control-input" name="cartao" id="cartao-cd">
                                <label class="custom-control-label" for="cartao-cd">Cartão de débito</label>
                            </div>
                        </div>
                        <div class="col-md-6 p-1">
                            <div class="form-group">
                                <input type="text" class="form-control border-light" placeholder="Número do cartão" required>
                            </div>
                        </div>
                        <div class="col-md-6 p-1">
                            <div class="form-group">
                                <input type="text" class="form-control border-light" placeholder="Nome no cartão" required>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4 col-xl-3 p-1">
                            <div class="form-group">
                                <input type="text" class="form-control border-light" placeholder="Mês de validade" required>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4 col-xl-3 p-1">
                            <div class="form-group">
                                <input type="text" class="form-control border-light" placeholder="Ano de validade" required>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4 col-xl-3 p-1">
                            <div class="form-group">
                                <input type="text" class="form-control border-light" placeholder="Código de segurança" required>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-12 col-xl-3 p-1">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="termo">
                                <label class="custom-control-label" for="termo">Aceito os <a href="#modal-termos" data-toggle="modal" data-dismiss="modal" class="text-underline">termos</a></label>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row mx-n1">
                        <div class="col col-xl-6 p-1">
                            <div class="form-group">
                                <input type="text" class="form-control border-light" placeholder="Adicionar cupom de desconto" required>
                            </div>
                        </div>
                        <div class="col-auto col-xl-3 p-1">
                            <a href="#" class="btn btn-block btn-secondary">Aplicar</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 col-lg-4 text-center mt-4 mt-md-0">
                    <div class="fixed">
                        <div class="bg-primary rounded text-white box-shadow p-2 p-xl-4">
                            <h3 class="title text-secondary mb-2">Formas de pagamento</h3>
                            <strong>Pagamento Aluguel</strong>
                            <div class="form-group mt-1">
                                <select class="cs-select cs-select--wrap maw-100">
                                    <option value="" disabled selected>R$450,00 em 5x de R$90,00</option>
                                    <option value="opcao1">Opção 1</option>
                                    <option value="opcao2">Opção 2</option>
                                    <option value="opcao3">Opção 3</option>
                                </select>
                            </div>
                            <strong>Pagamento seguro (à vista)</strong>
                            <div class="form-group mt-1">
                                <input type="text" class="form-control noreadonly" value="R$1050,00" readonly>
                            </div>
                            <button type="submit" href="#" class="btn px-2 btn-block btn-secondary">Finalizar reserva</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>

    <!-- Modal especificações -->
    <div class="modal fade" id="modal-espec" tabindex="-1" role="dialog" aria-labelledby="modal-espec-title" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="title mb-0" id="modal-espec-title">AS50 Campervan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="far fa-times text-primary"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Earum eligendi ab quis ullam. Soluta praesentium iusto, dolore quidem aliquam facere aliquid quae tempore dolorum, temporibus, ad aspernatur aperiam dolor mollitia? ipsum, dolor sit amet consectetur adipisicing elit. Explicabo, laborum. Praesentium nostrum soluta nesciunt odit laudantium, nihil ipsa maxime sapiente dolore corrupti voluptate minima omnis quos reiciendis corporis modi. Officiis. ipsum, dolor sit amet consectetur adipisicing elit. Nesciunt culpa quod numquam reiciendis! Deserunt non aliquam dolores saepe rem veritatis molestiae consectetur, quaerat error eum omnis, beatae repellendus unde numquam!</p>
                    <hr>
                    <ul class="lista-detalhes">
                        <li>
                            <i class="fa fa-fw fa-users mr-1"></i>
                            <span>5 passageiros</span>
                        </li>
                        <li>
                            <i class="fa fa-fw fa-steering-wheel mr-1"></i>
                            <span>Manual</span>
                        </li>
                        <li>
                            <i class="fa fa-fw fa-faucet-drip mr-1"></i>
                            <span>Pia cozinha</span>
                        </li>
                        <li>
                            <i class="fa fa-fw fa-snowflake mr-1"></i>
                            <span>Ar condicionado</span>
                        </li>  
                        <li>
                            <i class="fa fa-fw fa-bed-alt mr-1"></i>
                            <span>5 camas</span>
                        </li>
                        <li>
                            <i class="far fa-fw fa-th-large mr-1"></i>
                            <span>Cooktop</span>
                        </li>
                        <li>
                            <i class="fa fa-fw fa-refrigerator mr-1"></i>
                            <span>Refrigerador</span>
                        </li>
                        <li>
                            <i class="fa fa-fw fa-shower mr-1"></i>
                            <span>Banheiro</span>
                        </li>   
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <?php include('inc_rodape.php'); ?>
</body>
</html>