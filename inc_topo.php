<header id="topo" class="header-top sticky-top d-flex align-items-center">
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <input id="check-nav" type="checkbox" class="d-none"/>
                <div class="col col-md-auto">
                    <a href="index.php" class="logo">
                        <img src="assets/images/logo.png" alt="Vai de Motorhome" title="Vai de Motorhome">
                    </a>
                </div>
                <div class="col-auto px-0 px-xl-2">
                    <nav id="navbar-top" class="d-flex flex-column justify-content-between pt-4 pb-6 py-lg-0">
                        <ul class="nav">
                            <li class="nav-item"><a href="alugue.php" class="nav-link">Alugue</a></li>
                            <li class="nav-item"><a href="destinos.php" class="nav-link">Destinos</a></li>
                            <li class="nav-item"><a href="calculadora.php" class="nav-link">Calculadora</a></li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" role="button" id="dropdown-viagens" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Viagens em grupo</a>
                                <div class="dropdown-menu" aria-labelledby="dropdown-viagens">
                                    <a href="viagens-em-grupo.php" class="dropdown-item">Grupo Europa</a>
                                    <a href="viagens-em-grupo.php" class="dropdown-item">Grupo EUA</a>
                                </div>
                            </li>
                            <li class="nav-item"><a href="faq.php" class="nav-link">FAQ</a></li>
                            <li class="nav-item dropdown ml-xl-8">
                                <a class="nav-link dropdown-toggle" href="#" role="button" id="dropdown-moeda" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Moeda</a>
                                <div class="dropdown-menu" aria-labelledby="dropdown-moeda">
                                    <a href="#" class="dropdown-item">Real (R$)</a>
                                    <a href="#" class="dropdown-item">Dólar (US$)</a>
                                    <a href="#" class="dropdown-item">Euro (€)</a>
                                    <a href="#" class="dropdown-item">Libra (£)</a>
                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" role="button" id="dropdown-conta" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Conta</a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-conta">
                                    <a href="minha-conta.php" class="dropdown-item">Minha conta</a>
                                    <a href="minha-conta.php#meus-pedidos" class="dropdown-item">Meus pedidos</a>
                                    <a href="#modal-cadastro" class="dropdown-item" data-toggle="modal">Fazer cadastro</a>
                                    <a href="#modal-login" class="dropdown-item" data-toggle="modal">Login</a>
                                    <a href="" class="dropdown-item text-white bg-danger">Logout</a>
                                </div>
                            </li>
                        </ul>
                        <div class="d-lg-none mt-4 px-2">
                            <strong class="title d-block mb-4">Ficou com alguma dúvida? <br> Fale conosco pelo chat!</strong>
                            <a href="contato.php" class="btn btn-big-icon btn-primary">Entrar em contato</a>
                        </div>
                    </nav>
                </div>
                <div class="col-auto d-lg-none">
                    <label for="check-nav" class="btn-menu"><span></span></label>
                    <label for="check-nav" class="overlay-menu"></label>
                </div>
            </div>
        </div>
    </header>
    
    <div class="modal fade" id="modal-login" tabindex="-1" role="dialog" aria-labelledby="modal-login-title" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">
            <div class="modal-content bg-primary text-secondary">
                <div class="modal-header">
                    <h5 class="title font-size-xl text-center w-100 mb-0" id="modal-login-title">Login</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fal fa-times text-white"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Usuário, e-mail ou CPF" required />
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" placeholder="Senha" required />
                        </div>
                        <div class="row align-items-center flex-sm-row-reverse">
                            <div class="col-sm-auto">
                                <button type="submit" class="btn btn-block btn-secondary">Entrar</button>
                            </div>
                            <div class="col-sm small mt-3 mt-sm-0">
                                <p>Ainda não é cadastrado? <a href="#modal-cadastro" data-dismiss="modal" data-toggle="modal" class="text-secondary">Registre-se</a></p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="modal-cadastro" tabindex="-1" role="dialog" aria-labelledby="modal-cadastro-title" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="title mb-0" id="modal-cadastro-title">Registre-se</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fal fa-times text-primary"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <h2 class="title font-size-base">Seus dados</h2>
                        <div class="row mx-n1">
                            <div class="col-md-12 col-lg-4 p-1">
                                <div class="form-group">
                                    <input type="text" class="form-control border-light" placeholder="Nome completo" required>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-4 p-1">
                                <div class="form-group">
                                    <input type="email" class="form-control border-light" placeholder="E-mail" required>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-4 p-1">
                                <div class="form-group">
                                    <input type="date" class="form-control border-light" placeholder="Nascimento" />
                                    <i class="far fa-calendar-alt form-icon text-primary"></i>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-4 p-1">
                                <div class="form-group">
                                    <input type="text" class="form-control border-light" placeholder="Endereço" required>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-3 p-1">
                                <div class="form-group">
                                    <input type="text" class="form-control border-light" placeholder="Cidade" required>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-3 p-1">
                                <div class="form-group">
                                    <select class="cs-select cs-select--wrap border-light">
                                        <option value="" disabled selected>Estado</option>
                                        <option value="opcao1">São Paulo</option>
                                        <option value="opcao2">Bahia</option>
                                        <option value="opcao2">Minas Gerais</option>
                                        <option value="opcao3">Mato Grosso do Sul</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-2 p-1">
                                <div class="form-group">
                                    <input type="tel" class="form-control border-light mask-cep" placeholder="CEP" required>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-3 p-1">
                                <div class="form-group">
                                    <input type="tel" class="form-control border-light mask-tel" placeholder="Celular" required>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-3 p-1">
                                <div class="form-group">
                                    <input type="tel" class="form-control border-light mask-tel" placeholder="Telefone" required>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-3 p-1">
                                <div class="form-group">
                                    <input type="text" class="form-control border-light" placeholder="RG" required>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-3 p-1">
                                <div class="form-group">
                                    <input type="text" class="form-control border-light" placeholder="CPF" required>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <h2 class="title font-size-base">Dados de pagamento</h2>
                        <div class="row mx-n1 align-items-center">
                            <div class="col-12 p-1">
                                <div class="custom-control custom-radio d-inline-block">
                                    <input type="radio" class="custom-control-input" name="cartao" id="modal-cartao-cc">
                                    <label class="custom-control-label" for="modal-cartao-cc">Cartão de crédito</label>
                                </div>
                                <div class="custom-control custom-radio d-sm-inline-block mt-2 mt-sm-0 ml-sm-4">
                                    <input type="radio" class="custom-control-input" name="cartao" id="modal-cartao-cd">
                                    <label class="custom-control-label" for="modal-cartao-cd">Cartão de débito</label>
                                </div>
                            </div>
                            <div class="col-md-6 p-1">
                                <div class="form-group">
                                    <input type="text" class="form-control border-light" placeholder="Número do cartão" required>
                                </div>
                            </div>
                            <div class="col-md-6 p-1">
                                <div class="form-group">
                                    <input type="text" class="form-control border-light" placeholder="Nome no cartão" required>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-3 p-1">
                                <div class="form-group">
                                    <input type="text" class="form-control border-light" placeholder="Mês de validade" required>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-3 p-1">
                                <div class="form-group">
                                    <input type="text" class="form-control border-light" placeholder="Ano de validade" required>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-3 p-1">
                                <div class="form-group">
                                    <input type="text" class="form-control border-light" placeholder="Código de segurança" required>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-3 p-1">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="termo-modal">
                                    <label class="custom-control-label" for="termo-modal">Aceito os <a href="#modal-termos" data-toggle="modal" class="text-underline">termos</a></label>
                                </div>
                            </div>
                            <div class="col-md-6 offset-md-6 col-lg-3 offset-lg-9 p-1">
                                <button type="submit" class="btn btn-block btn-secondary">Registrar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="modal-termos" tabindex="-1" role="dialog" aria-labelledby="modal-termos-title" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="title mb-0" id="modal-termos-title">Termos Gerais</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fal fa-times"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla cursus fringilla diam et convallis. Curabitur sed tortor at sem tempor finibus vitae eu enim. Quisque quis nisi iaculis, condimentum risus eu, gravida quam. Aliquam et eros malesuada, porttitor ante eget, tristique nibh. Nulla facilisi. Aliquam faucibus odio quam, faucibus finibus quam faucibus non. In congue, ipsum in ornare sodales, augue arcu egestas justo, eu iaculis lorem ligula ac lectus. In at elementum orci, at venenatis lectus. Phasellus at orci a dolor vulputate sodales. Pellentesque bibendum enim ut ante cursus, a dapibus ante rhoncus. Suspendisse sagittis dui in posuere congue. Ut efficitur eget metus at ultricies. Aliquam egestas metus sed quam rutrum, vitae dictum mauris feugiat. Sed ac metus pellentesque, congue diam id, maximus nisi. Sed feugiat, magna vel malesuada auctor, ipsum elit venenatis nisl, et scelerisque augue metus iaculis eros.</p>
                    <p>Ut id ligula id magna sagittis pulvinar. Ut vel enim eget nibh mattis sagittis. Sed sit amet egestas magna. Donec porttitor lacus sed leo iaculis, in porttitor quam malesuada. Nullam vitae dui dui. Mauris vel mauris vitae massa pulvinar faucibus. Proin feugiat pulvinar sem id convallis. Cras ac maximus dolor, et rutrum sapien. Quisque placerat egestas convallis. Ut at enim faucibus, porta augue sed, consectetur magna. Nulla congue cursus magna. Phasellus egestas tempor ligula nec finibus</p>
                    <p>Praesent pulvinar eu nisi quis egestas. Morbi a commodo erat. Suspendisse potenti. Donec eu mauris lobortis, semper orci eu, viverra turpis. Vestibulum tellus lorem, rutrum non augue ut, pretium condimentum ligula. Pellentesque sodales, nibh eget vestibulum ornare, justo urna tempor ex, et tincidunt dui nunc eget quam. Maecenas dapibus auctor erat, eget iaculis ligula elementum a. Cras rutrum fringilla nibh vel lobortis. Nulla eleifend magna est, ac placerat dui posuere at. Curabitur viverra tortor massa, id aliquet dolor finibus sed. Praesent porttitor odio ut nisi aliquam, eu dignissim nibh semper. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                    <p>In tempus laoreet nisi, ac elementum est dictum quis. In vitae placerat massa. Proin convallis tempor nisi, nec egestas nulla. Phasellus dignissim diam nec purus feugiat iaculis. Ut vitae ullamcorper nisi. Integer venenatis diam enim, et volutpat nibh consectetur et. Phasellus mattis accumsan nulla, et faucibus orci gravida at.</p>
                    <p>Nam nec risus posuere, euismod tellus at, tempus enim. Suspendisse eget bibendum ligula. Vestibulum a magna at massa dapibus ullamcorper ut et dolor. Aenean accumsan viverra placerat. Nam molestie lobortis sapien id scelerisque. Phasellus pretium mauris at vulputate feugiat. Suspendisse arcu dolor, cursus at quam sed, tempor sodales risus. Nam vulputate non elit eget tempus. Sed laoreet metus a orci suscipit, nec hendrerit magna mollis. Donec neque urna, viverra eget dignissim et, sodales nec diam. Curabitur suscipit, quam vitae imperdiet porttitor, neque diam dictum leo, nec elementum odio est fermentum massa. Morbi et finibus enim, et ullamcorper sem. Nullam semper tincidunt felis eu porttitor.risus. Duis feugiat, ante congue tristique condimentum, tellus tortor facilisis nulla, ac suscipit ante elit finibus nisi.</p>
                </div>
            </div>
        </div>
    </div>