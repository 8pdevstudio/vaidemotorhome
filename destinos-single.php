<!doctype html>
<html lang="pt-br">
<head>
    <?php include('inc_head.php'); ?>
</head>
<body>
    <?php include('inc_topo.php'); ?>

    <section class="py-4 py-md-7 sem-contato-rodape">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 pr-lg-0">
                    <div class="item-motorhome py-2 mb-6 mb-md-4">
                        <div class="col-md-9 px-md-0">
                            <div class="row align-items-center">
                                <div class="col-12 col-lg-5 border-right-lg border-light mb-3 mb-lg-0">
                                    <h1 class="title">Viaje para Siena, Itália!</h1>
                                    <strong class="d-block mb-2">Informações gerais</strong>
                                    <div class="small">
                                        <p>Lorem ipsum dolor sit amet conse incidunt laudantium dolorum quaerat distinctio quidem asperiores cupiditate quos ducimus. Cumque, minus aliquam!</p>
                                    </div>
                                </div>
                                <div class="col-8 col-lg-4 duas-colunas">
                                    <strong class="d-block mb-1">Rotas Semelhantes</strong>
                                    <ul class="small">
                                        <li class="pt-1">Veneza</li>
                                        <li class="pt-1">Bolonha</li>
                                        <li class="pt-1">Siena</li>
                                        <li class="pt-1">Berlim</li>
                                        <li class="pt-1">Nápoles</li>
                                        <li class="pt-1">Paris</li>
                                        <li class="pt-1">Pisa</li>
                                        <li class="pt-1">Alpes Suíços</li>
                                    </ul>
                                </div>
                                <div class="col-4 col-lg-3">
                                    <strong class="d-block mb-1">Melhores meses</strong>
                                    <ul class="small">
                                        <li class="pt-1">Janeiro</li>
                                        <li class="pt-1">Abril</li>
                                        <li class="pt-1">Junho</li>
                                        <li class="pt-1">Agosto</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 pr-md-0">
                            <div class="bg-primary rounded box-shadow text-secondary text-center p-2 mr-md-n4 mb-n5 mt-3 my-md-0 position-relative small">
                                <h4 class="text-white title mb-0">78 motorhomes</h4>
                                <p>Disponíveis</p>
                                <hr class="my-3">
                                <p class="mb-1">Média de valores: R$175,00 / diária</p>
                                <p>Viaje até 20 dias</p>
                            </div>
                        </div>
                    </div>
                    <div class="galeria">
                        <div class="galeria-grande">
                            <div>
                                <a href="assets/images/galeria-grande.jpg" data-thumb="assets/images/galeria-pequena.jpg" data-fancybox="galeria" class="ratio-img ratio-img-16by9">
                                    <img src="assets/images/galeria-grande.jpg" alt="Foto 1" title="Foto 1">
                                </a>
                            </div>
                            <div>
                                <a href="assets/images/bg-destinos.jpg" data-thumb="assets/images/bg-destinos.jpg" data-fancybox="galeria" class="ratio-img ratio-img-16by9">
                                    <img src="assets/images/bg-destinos.jpg" alt="Foto 1" title="Foto 1">
                                </a>
                            </div>
                            <div>
                                <a href="assets/images/fundo-interna.jpg" data-thumb="assets/images/fundo-interna.jpg" data-fancybox="galeria" class="ratio-img ratio-img-16by9">
                                    <img src="assets/images/fundo-interna.jpg" alt="Foto 1" title="Foto 1">
                                </a>
                            </div>
                            <div>
                                <a href="assets/images/quem-somos.jpg" data-thumb="assets/images/quem-somos.jpg" data-fancybox="galeria" class="ratio-img ratio-img-16by9">
                                    <img src="assets/images/quem-somos.jpg" alt="Foto 1" title="Foto 1">
                                </a>
                            </div>
                            <div>
                                <a href="assets/images/vantagens.jpg" data-thumb="assets/images/vantagens.jpg" data-fancybox="galeria" class="ratio-img ratio-img-16by9">
                                    <img src="assets/images/vantagens.jpg" alt="Foto 1" title="Foto 1">
                                </a>
                            </div>
                            <div>
                                <a href="assets/images/motorhome.jpg" data-thumb="assets/images/motorhome.jpg" data-fancybox="galeria" class="ratio-img ratio-img-16by9">
                                    <img src="assets/images/motorhome.jpg" alt="Foto 1" title="Foto 1">
                                </a>
                            </div>
                        </div>
                        <div class="galeria-pequena">
                            <div class="mx-2">
                                <div class="ratio-img ratio-img-16by9">
                                    <img src="assets/images/galeria-pequena.jpg" alt="Foto 1" title="Foto 1">
                                </div>
                            </div>
                            <div class="mx-2">
                                <div class="ratio-img ratio-img-16by9">
                                    <img src="assets/images/bg-destinos.jpg" alt="Foto 1" title="Foto 1">
                                </div>
                            </div>
                            <div class="mx-2">
                                <div class="ratio-img ratio-img-16by9">
                                    <img src="assets/images/fundo-interna.jpg" alt="Foto 1" title="Foto 1">
                                </div>
                            </div>
                            <div class="mx-2">
                                <div class="ratio-img ratio-img-16by9">
                                    <img src="assets/images/quem-somos.jpg" alt="Foto 1" title="Foto 1">
                                </div>
                            </div>
                            <div class="mx-2">
                                <div class="ratio-img ratio-img-16by9">
                                    <img src="assets/images/vantagens.jpg" alt="Foto 1" title="Foto 1">
                                </div>
                            </div>
                            <div class="mx-2">
                                <div class="ratio-img ratio-img-16by9">
                                    <img src="assets/images/motorhome.jpg" alt="Foto 1" title="Foto 1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="bg-primary text-white pt-2 px-2 pt-md-4 px-md-4 pb-2 rounded text-center mt-4">
                        <h3 class="title text-secondary">Guia turístico de Siena</h3>
                        <hr class="border-secondary">
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Atque debitis illum totam, vero reiciendis cumque molestias voluptatibus ullam provident nam quos aperiam, fugit non suscipit porro quasi quisquam corrupti temporibus?</p>
                        <div class="accordion-buttons d-flex justify-content-between mt-4">
                            <button class="btn" type="button" data-toggle="collapse" data-target="#guia-1" aria-expanded="true" aria-controls="guia-1">1. Conheça a região</button>
                            <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#guia-2" aria-expanded="true" aria-controls="guia-2">2. Atrativos</button>
                            <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#guia-3" aria-expanded="true" aria-controls="guia-3">3. Planejando sua viagem</button>
                            <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#guia-4" aria-expanded="true" aria-controls="guia-4">4. Busque um motorhome</button>
                        </div>
                    </div>
                    <div class="border border-light rounded p-2 mt-4">
                        <div id="guia-turistico" class="accordion-content py-2 pr-4 pl-2">
                            <div id="guia-1" class="collapse show" data-parent="#guia-turistico">
                                <h3 class="title">Lorem Ipsum</h3>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Expedita quis facere nam consectetur suscipit perferendis in autem, dolorum ipsum commodi labore culpa voluptas ullam cum adipisci minus voluptatem iste blanditiis.</p>
                                <div class="row my-2 mx-n1">
                                    <div class="col-sm-4 p-1">
                                        <a href="assets/images/destino.jpg" data-fancybox="galeria-texto" data-thumb="assets/images/destino.jpg" class="ratio-img ratio-img-4by3 rounded">
                                            <img src="assets/images/destino.jpg" alt="Título da imagem" title="Título da imagem">
                                        </a>
                                    </div>
                                    <div class="col-sm-4 p-1">
                                        <a href="assets/images/destino.jpg" data-fancybox="galeria-texto" data-thumb="assets/images/destino.jpg" class="ratio-img ratio-img-4by3 rounded">
                                            <img src="assets/images/destino.jpg" alt="Título da imagem" title="Título da imagem">
                                        </a>
                                    </div>
                                    <div class="col-sm-4 p-1">
                                        <a href="assets/images/destino.jpg" data-fancybox="galeria-texto" data-thumb="assets/images/destino.jpg" class="ratio-img ratio-img-4by3 rounded">
                                            <img src="assets/images/destino.jpg" alt="Título da imagem" title="Título da imagem">
                                        </a>
                                    </div>
                                </div>
                                <h3 class="title">Lorem Ipsum</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam a turpis quis velit molestie lacinia nec a felis. Morbi dapibus augue non tempus tincidunt. Nunc eget vulputate nulla, ac porttitor quam. Vivamus finibus, justo eu lacinia vestibulum, justo tortor volutpat nibh, at suscipit nisl quam ut nulla. Nulla dui mi, pulvinar nec viverra in, varius quis magna. Vivamus dignissim purus lorem, ac volutpat tortor vestibulum bibendum. Fusce orci ligula, faucibus id vulputate sed, elementum ac ipsum. Mauris consectetur, neque eu ultricies mattis, nunc urna vehicula risus, quis gravida ligula metus sit amet risus. Praesent sit amet hendrerit sapien.</p>
                                <p>Cras in rutrum odio. Cras a odio a libero gravida aliquam. Sed vel sapien ligula. Praesent non nulla et diam varius pulvinar eget at sem. Donec non neque id nisl ullamcorper suscipit quis non massa. Vestibulum nibh ipsum, facilisis auctor rhoncus a, dignissim non nisi. Morbi eget egestas sapien.</p>
                            </div>
                            <div id="guia-2" class="collapse" data-parent="#guia-turistico">
                            2. Atrativos
                            </div>
                            <div id="guia-3" class="collapse" data-parent="#guia-turistico">
                            3. Planejando sua viagem
                            </div>
                            <div id="guia-4" class="collapse overflow-hidden" data-parent="#guia-turistico">
                                <div class="bg-primary text-white rounded pt-2 px-2 pb-5 position-relative row mx-0 align-items-center mb-9">
                                    <div class="col-md-12 p-1">
                                        <div class="form-group">
                                            <select class="cs-select cs-select--wrap maw-100">
                                                <option value="" disabled selected>País</option>
                                                <option value="opcao1">Opção 1Opção 1Opção 1Opção 1Opção 1Opção 1Opção 1</option>
                                                <option value="opcao2">Opção 1</option>
                                                <option value="opcao2">Opção 2</option>
                                                <option value="opcao3">Opção 3</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 p-1">
                                        <div class="form-group">
                                            <select class="cs-select cs-select--wrap maw-100">
                                                <option value="" disabled selected>Retirar em</option>
                                                <option value="opcao1">Opção 1Opção 1Opção 1Opção 1Opção 1Opção 1Opção 1</option>
                                                <option value="opcao1">Opção 1</option>
                                                <option value="opcao2">Opção 2</option>
                                                <option value="opcao3">Opção 3</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 p-1">
                                        <div class="form-group">
                                            <select class="cs-select cs-select--wrap maw-100">
                                                <option value="" disabled selected>Entregar em</option>
                                                <option value="opcao1">Opção 1Opção 1Opção 1Opção 1Opção 1Opção 1Opção 1</option>
                                                <option value="opcao1">Opção 1</option>
                                                <option value="opcao2">Opção 2</option>
                                                <option value="opcao3">Opção 3</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 p-1">
                                        <div class="form-group">
                                            <input type="date" class="form-control" placeholder="Data da retirada" />
                                            <i class="far fa-calendar-alt form-icon"></i>
                                        </div>
                                    </div>
                                    <div class="col-md-6 p-1">
                                        <div class="form-group">
                                            <input type="date" class="form-control" placeholder="Data da entrega" />
                                            <i class="far fa-calendar-alt form-icon"></i>
                                        </div>
                                    </div>
                                    <div class="col-md-12 p-1">
                                        <div class="form-group">
                                            <div class="custom-control custom-switch">
                                                <input type="checkbox" class="custom-control-input" id="possui-hab-int">
                                                <label class="custom-control-label" for="possui-hab-int">Possui habilitação internacional</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-7 col-xl-6 p-1">
                                            <div class="form-group">
                                                <select class="cs-select cs-select--wrap cs-select--short maw-100">
                                                    <option value="" disabled selected>País da licença para dirigir</option>
                                                <option value="opcao1">Opção 1Opção 1Opção 1Opção 1Opção 1Opção 1Opção 1</option>
                                                <option value="opcao1">Opção 1</option>
                                                <option value="opcao2">Opção 2</option>
                                                <option value="opcao3">Opção 3</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-5 col-xl-6 p-1">
                                        <div class="form-group">
                                            <input type="text" class="form-control bg-white" value="Idade" readonly />
                                            <div class="form-icon">
                                                <div class="input-group input-number input-number-sm">
                                                    <div class="input-group-prepend">
                                                        <button class="btn text-primary btn-minus" type="button"><i class="far fa-minus"></i></button>
                                                    </div>
                                                    <input type="number" class="form-control" value="18" min="18" step="1" readonly>
                                                    <div class="input-group-append">
                                                        <button class="btn text-primary btn-plus" type="button"><i class="far fa-plus"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <button class="btn btn-secondary btn-center-bottom box-shadow-none" type="submit">Buscar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <a href="alugue.php" class="btn btn-link btn-arrow-left">Voltar para a busca</a>
                    <hr class="my-6">
                    <h3 class="title">Comentários e avaliações sobre o motorhome</h3>
                    <ul class="scrollable-md">
                        <li class="border border-light p-4 rounded px-2 my-lg-4">
                            <strong class="font-size-lg">Renan Souza - "Excelente!"</strong>
                            <strong class="d-block mb-2">alugou no dia 12/10/2019</strong>
                            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Tempora recusandae quis aspernatur maiores eveniet perferendis voluptas nisi, ut doloremque corrupti beatae consequuntur provident voluptatibus repellendus. Commodi molestias laudantium et consequuntur!</p>
                        </li>
                        <li class="border border-light p-4 rounded px-2 my-lg-4">
                            <strong class="font-size-lg">Renan Souza - "Excelente!"</strong>
                            <strong class="d-block mb-2">alugou no dia 12/10/2019</strong>
                            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Tempora recusandae quis aspernatur maiores eveniet perferendis voluptas nisi, ut doloremque corrupti beatae consequuntur provident voluptatibus repellendus. Commodi molestias laudantium et consequuntur!</p>
                        </li>
                        <li class="border border-light p-4 rounded px-2 my-lg-4">
                            <strong class="font-size-lg">Renan Souza - "Excelente!"</strong>
                            <strong class="d-block mb-2">alugou no dia 12/10/2019</strong>
                            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Tempora recusandae quis aspernatur maiores eveniet perferendis voluptas nisi, ut doloremque corrupti beatae consequuntur provident voluptatibus repellendus. Commodi molestias laudantium et consequuntur!</p>
                        </li>
                    </ul>
                    <hr class="my-6">
                    <h3 class="title text-center text-sm-left"><span class="d-block d-sm-inline-block">Deixe seu comentário!</span> <a href="login.php" class="btn btn-secondary mt-3 mt-sm-0 ml-sm-6 va-initial">Faça login</a></h3>
                    <form class="mt-4">
                        <div class="row mx-n1">
                            <div class="col-md-4 p-1">
                                <div class="form-group">
                                    <input type="text" class="form-control border-light" placeholder="Nome" required>
                                </div>
                            </div>
                            <div class="col-md-4 p-1">
                                <div class="form-group">
                                    <input type="text" class="form-control border-light" placeholder="Título" required>
                                </div>
                            </div>
                            <div class="col-md-4 p-1">
                                <div class="form-group">
                                    <input type="date" class="form-control border-light" placeholder="Data da compra" />
                                    <i class="far fa-calendar-alt form-icon text-primary"></i>
                                </div>
                            </div>
                            <div class="col-12 p-1">
                                <div class="form-group">
                                    <textarea class="form-control border-light" placeholder="Comentário" required></textarea>
                                </div>
                            </div>
                            <div class="col-md-4 p-1">
                                <button type="submit" class="btn btn-block btn-secondary">Postar</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-4 pl-lg-6 text-center mt-8 mt-lg-0">
                    <div class="fixed pb-4">
                        <div class="bg-secondary rounded box-shadow p-4">
                            <h3 class="title mb-6">Porque viajar com o Vai de Motorhome?</h3>
                            <ul class="scrollable-md">
                                <li class="px-1 px-md-0 mt-md-4">
                                    <div class="border border-primary border-width-2 p-1 fz-0 d-inline-block rounded-circle">
                                        <div class="ratio-img ratio-img-avatar bg-white rounded-circle">
                                            <i class="fal fa-user-headset"></i>
                                        </div>
                                    </div>
                                    <strong class="d-block my-1">Atendimento personalizado</strong>
                                    <p>Sabemos que viajar de motorhome é novidade para muita gente. Por isso temos uma equipe a disposição para esclarecer qualquer dúvida.</p>
                                </li>
                                <li class="px-1 px-md-0 mt-md-4">
                                    <div class="border border-primary border-width-2 p-1 fz-0 d-inline-block rounded-circle">
                                        <div class="ratio-img ratio-img-avatar bg-white rounded-circle">
                                            <i>R$</i>
                                        </div>
                                    </div>
                                    <strong class="d-block my-1">Pagamento em Reais</strong>
                                    <p>O Pagamento é em reais. Assim você não corre o risco de sofrer com alterações cambiais.</p>
                                </li>
                                <li class="px-1 px-md-0 mt-md-4">
                                    <div class="border border-primary border-width-2 p-1 fz-0 d-inline-block rounded-circle">
                                        <div class="ratio-img ratio-img-avatar bg-white rounded-circle">
                                            <i class="fal fa-rv"></i>
                                        </div>
                                    </div>
                                    <strong class="d-block my-1">Seleção dos melhores Motorhomes</strong>
                                    <p>Selecionamos como frota somente motorhomes que já usamos e aprovamos.</p>
                                </li>
                                <li class="px-1 px-md-0 mt-md-4">
                                    <div class="border border-primary border-width-2 p-1 fz-0 d-inline-block rounded-circle">
                                        <div class="ratio-img ratio-img-avatar bg-white rounded-circle">
                                            <i class="fal fa-users"></i>
                                        </div>
                                    </div>
                                    <strong class="d-block my-1">Feito por quem entende de  viagens de Motorhome</strong>
                                    <p>Nossos fundadores já viajaram em inúmeros modelos de motorhome, tanto que escolheram um para viver em tempo integral.</p>
                                </li>
                                <li class="px-1 px-md-0 mt-md-4">
                                    <div class="border border-primary border-width-2 p-1 fz-0 d-inline-block rounded-circle">
                                        <div class="ratio-img ratio-img-avatar bg-white rounded-circle">
                                            <i class="fal fa-language"></i>
                                        </div>
                                    </div>
                                    <strong class="d-block my-1">Instruções de retirada em português</strong>
                                    <p>Todas as Instruções para retirada do motorhome em Português.</p>
                                </li>
                            </ul>
                        </div>
                        <div class="row justify-content-center mt-6 mx-n1">
                            <div class="col-auto p-1">
                                <span class="badge badge-pill badge-primary">família</span>
                            </div>
                            <div class="col-auto p-1">
                                <span class="badge badge-pill badge-primary">europeu</span>
                            </div>
                            <div class="col-auto p-1">
                                <span class="badge badge-pill badge-primary">casal</span>
                            </div>
                            <div class="col-auto p-1">
                                <span class="badge badge-pill badge-primary">automático</span>
                            </div>
                            <div class="col-auto p-1">
                                <span class="badge badge-pill badge-primary">conforto</span>
                            </div>
                        </div>
                        <hr class="my-6">
                        <h4 class="title mb-4">Ficou alguma dúvida? <br>Fale conosco pelo chat!</h4>
                        <a href="contato.php" class="btn btn-big-icon btn-primary">Entrar em contato</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php include('inc_rodape.php'); ?>
</body>
</html>