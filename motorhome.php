<!doctype html>
<html lang="pt-br">
<head>
    <?php include('inc_head.php'); ?>
</head>
<body>
    <?php include('inc_topo.php'); ?>

    <section class="py-4 py-md-7 sem-contato-rodape">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 pr-lg-0">
                    <div class="item-motorhome py-2 mb-6 mb-md-4">
                        <div class="col-md-8 col-xl-9 px-md-0">
                            <div class="row align-items-center">
                                <div class="col-md-4">
                                    <div class="ratio-img ratio-img-4by3">
                                        <img src="assets/images/motorhome.jpg" alt="AS50 Campervan" title="AS50 Campervan">
                                    </div>
                                </div>
                                <div class="col">
                                    <h4 class="title">AS50 Campervan</h4>
                                    <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi dolore riatur repudiandae!</p>
                                </div>
                                <div class="col-12 col-xl-auto text-center text-xl-right mt-2 mt-xl-0">
                                    <a href="assets/images/galeria-grande.jpg" data-fancybox="as50-campervan-2" data-thumb="assets/images/galeria-pequena.jpg" class="btn btn-link">Ver fotos<i class="fad fa-plus-circle text-secondary ml-1"></i></a>
                                    <div class="d-none">
                                        <a href="assets/images/galeria-grande.jpg" data-fancybox="as50-campervan-2" data-thumb="assets/images/galeria-pequena.jpg"></a>
                                        <a href="assets/images/galeria-grande.jpg" data-fancybox="as50-campervan-2" data-thumb="assets/images/galeria-pequena.jpg"></a>
                                        <a href="assets/images/galeria-grande.jpg" data-fancybox="as50-campervan-2" data-thumb="assets/images/galeria-pequena.jpg"></a>
                                        <a href="assets/images/galeria-grande.jpg" data-fancybox="as50-campervan-2" data-thumb="assets/images/galeria-pequena.jpg"></a>
                                    </div>
                                    <br class="d-none d-xl-block">
                                    <button type="button" class="btn btn-link ml-3 ml-xl-0" data-toggle="modal" data-target="#modal-espec">Especificações<i class="fad fa-plus-circle text-secondary ml-1"></i></button>
                                </div>
                            </div>
                            <hr>
                            <ul class="lista-detalhes row mx-n1">
                                <li class="col-6 col-sm-4 col-md-6 col-xl-3 px-1 d-flex">
                                    <i class="fa fa-fw fa-users mr-1"></i>
                                    <span>5 passageiros</span>
                                </li>
                                <li class="col-6 col-sm-4 col-md-6 col-xl-3 px-1 d-flex">
                                    <i class="fa fa-fw fa-steering-wheel mr-1"></i>
                                    <span>Manual</span>
                                </li>
                                <li class="col-6 col-sm-4 col-md-6 col-xl-3 px-1 d-flex">
                                    <i class="fa fa-fw fa-faucet-drip mr-1"></i>
                                    <span>Pia cozinha</span>
                                </li>
                                <li class="col-6 col-sm-4 col-md-6 col-xl-3 px-1 d-flex">
                                    <i class="fa fa-fw fa-snowflake mr-1"></i>
                                    <span>Ar condicionado</span>
                                </li>  
                                <li class="col-6 col-sm-4 col-md-6 col-xl-3 px-1 d-flex">
                                    <i class="fa fa-fw fa-bed-alt mr-1"></i>
                                    <span>5 camas</span>
                                </li>
                                <li class="col-6 col-sm-4 col-md-6 col-xl-3 px-1 d-flex">
                                    <i class="far fa-fw fa-th-large mr-1"></i>
                                    <span>Cooktop</span>
                                </li>
                                <li class="col-6 col-sm-4 col-md-6 col-xl-3 px-1 d-flex">
                                    <i class="fa fa-fw fa-refrigerator mr-1"></i>
                                    <span>Refrigerador</span>
                                </li>
                                <li class="col-6 col-sm-4 col-md-6 col-xl-3 px-1 d-flex">
                                    <i class="fa fa-fw fa-shower mr-1"></i>
                                    <span>Banheiro</span>
                                </li>   
                            </ul>
                        </div>
                        <div class="col-md-4 col-xl-3 pr-md-0">
                            <div class="bg-primary rounded box-shadow text-secondary text-center p-3 mb-n5 mt-3 my-md-0 mr-md-n4 position-relative">
                                <h4 class="text-white title mb-0">R$450,00</h4>
                                <p>Incluso taxas e impostos</p>
                                <hr class="my-3">
                                <p>Diária: R$75,00</p>
                                <p>7 dias: R$450,00</p>
                            </div>
                        </div>
                    </div>
                    <div class="border border-light rounded py-2 pr-2 p-sm-4 mt-4 ml-3 ml-sm-0">
                        <ul class="lista-reserva">
                            <li class="row flex-nowrap">
                                <div class="col-auto px-0 px-sm-2 ml-n2 ml-sm-0">
                                    <div class="border border-primary border-width-2 bg-white p-1 fz-0 d-inline-block rounded-circle">
                                        <div class="ratio-img ratio-img-avatar-sm bg-secondary">
                                            <i class="far fa-rv"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="col pt-2 pt-sm-3 ml-n4 ml-sm-0">
                                    <strong class="d-block mb-4 font-size-lg pl-4 pl-sm-0">1. Confirme a cidade, data e hora da retirada e entrega</strong>
                                    <span><i class="fa fa-fw fa-map-marker-alt mr-1"></i> Retirando em <b>Roma</b></span>
                                    <a class="btn btn-link ml-1 ml-sm-3"><i class="fa fa-map-marker-edit mr-1"></i> alterar</a>
                                    <hr class="dotted">
                                    <span><i class="fa fa-fw fa-map-marker-alt mr-1"></i> Entregando em <b>Roma</b></span>
                                    <a class="btn btn-link ml-1 ml-sm-3"><i class="fa fa-map-marker-edit mr-1"></i> alterar</a>
                                    <form class="mt-2">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label-abs d-lg-none">Data e hora da retirada</label>
                                                    <input type="date" data-time class="form-control" placeholder="Data e hora da retirada" />
                                                    <i class="far fa-calendar-alt form-icon text-primary"></i>
                                                </div>
                                            </div>
                                            <div class="col-md-6 mt-3 mt-md-0">
                                                <div class="form-group">
                                                    <label class="label-abs d-lg-none">Data e hora da entrega</label>
                                                    <input type="date" data-time class="form-control" placeholder="Data e hora da entrega" />
                                                    <i class="far fa-calendar-alt form-icon text-primary"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </li>
                            <li class="row flex-nowrap">
                                <div class="col-auto px-0 px-sm-2 ml-n2 ml-sm-0">
                                    <div class="border border-primary border-width-2 bg-white p-1 fz-0 d-inline-block rounded-circle">
                                        <div class="ratio-img ratio-img-avatar-sm bg-secondary rounded-circle">
                                            <i class="far fa-cart-plus"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="col pt-2 pt ml-n4 ml-sm-0">
                                    <strong class="d-block mb-4 font-size-lg pl-4 pl-sm-0">2. Itens extras</strong>
                                    <ul class="itens-extras">
                                        <li class="row align-items-center justify-content-between">
                                            <div class="col-12 col-sm mb-2 mb-sm-0">
                                                <strong>Lorem ipsum sit dalamet</strong>
                                            </div>
                                            <div class="col-auto">
                                                <div class="input-group input-number">
                                                    <div class="input-group-prepend">
                                                        <button class="btn text-primary btn-minus" type="button"><i class="far fa-minus"></i></button>
                                                    </div>
                                                    <input type="number" class="form-control" value="1" min="1" step="1" readonly>
                                                    <div class="input-group-append">
                                                        <button class="btn text-primary btn-plus" type="button"><i class="far fa-plus"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <strong>R$250,00</strong>
                                            </div>
                                        </li>
                                        <li class="row align-items-center justify-content-between">
                                            <div class="col-12 col-sm mb-2 mb-sm-0">
                                                <strong>Lorem ipsum sit dalamet</strong>
                                            </div>
                                            <div class="col-auto">
                                                <div class="input-group input-number">
                                                    <div class="input-group-prepend">
                                                        <button class="btn text-primary btn-minus" type="button"><i class="far fa-minus"></i></button>
                                                    </div>
                                                    <input type="number" class="form-control" value="1" min="1" step="1" readonly>
                                                    <div class="input-group-append">
                                                        <button class="btn text-primary btn-plus" type="button"><i class="far fa-plus"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <strong>R$250,00</strong>
                                            </div>
                                        </li>
                                        <li class="row align-items-center justify-content-between">
                                            <div class="col-12 col-sm mb-2 mb-sm-0">
                                                <strong>Lorem ipsum sit dalamet</strong>
                                            </div>
                                            <div class="col-auto">
                                                <div class="input-group input-number">
                                                    <div class="input-group-prepend">
                                                        <button class="btn text-primary btn-minus" type="button"><i class="far fa-minus"></i></button>
                                                    </div>
                                                    <input type="number" class="form-control" value="1" min="1" step="1" readonly>
                                                    <div class="input-group-append">
                                                        <button class="btn text-primary btn-plus" type="button"><i class="far fa-plus"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <strong>R$250,00</strong>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="row flex-nowrap">
                                <div class="col-auto px-0 px-sm-2 ml-n2 ml-sm-0">
                                    <div class="border border-primary border-width-2 bg-white p-1 fz-0 d-inline-block rounded-circle">
                                        <div class="ratio-img ratio-img-avatar-sm bg-secondary">
                                            <i class="far fa-shield"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="col pt-2 pt-sm-3 ml-n4 ml-sm-0">
                                    <strong class="d-block mb-4 font-size-lg pl-4 pl-sm-0">3. Seguro do veículo</strong>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod officiis voluptatum placeat distinctio praesentium dignissimos maxime non, quas beatae porro velit ratione, et quos saepe, asperiores iure at deleniti eius?</p>
                                    <div class="bg-light p-4 rounded d-flex flex-wrap justify-content-between align-items-center">
                                        <div class="form-group col-12 col-sm pl-0 m-sm-0">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="seguro-motohome">
                                                <label class="custom-control-label" for="seguro-motohome">Lorem ipsum sit dalamet</label>
                                            </div>
                                        </div>
                                        <div class="col-auto px-0">
                                            <strong>R$250,00</strong>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="row flex-nowrap">
                                <div class="col-auto px-0 px-sm-2 ml-n2 ml-sm-0">
                                    <div class="border border-primary border-width-2 bg-white p-1 fz-0 d-inline-block rounded-circle">
                                        <div class="ratio-img ratio-img-avatar-sm bg-secondary">
                                            <i class="far fa-info"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="col pt-2 pt-sm-3 ml-n4 ml-sm-0">
                                    <strong class="d-block mb-4 font-size-lg pl-4 pl-sm-0">4. Informações extras</strong>
                                    <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Consequatur molestias sint doloremque, numquam repudiandae distinctio maiores assumenda debitis magni fugiat delectus. Nisi, voluptate possimus aperiam labore qui adipisci. Quo, nisi.</p>
                                    <button type="submit" class="btn btn-secondary mt-4">Finalizar reserva</button>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <br>
                    <a href="alugue.php" class="btn btn-link btn-arrow-left">Voltar para a busca</a>
                    <hr class="my-6">
                    <h3 class="title">Comentários e avaliações sobre o motorhome</h3>
                    <ul class="scrollable-md">
                        <li class="border border-light p-4 rounded px-2 my-lg-4">
                            <strong class="font-size-lg">Renan Souza - "Excelente!"</strong>
                            <strong class="d-block mb-2">alugou no dia 12/10/2019</strong>
                            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Tempora recusandae quis aspernatur maiores eveniet perferendis voluptas nisi, ut doloremque corrupti beatae consequuntur provident voluptatibus repellendus. Commodi molestias laudantium et consequuntur!</p>
                        </li>
                        <li class="border border-light p-4 rounded px-2 my-lg-4">
                            <strong class="font-size-lg">Renan Souza - "Excelente!"</strong>
                            <strong class="d-block mb-2">alugou no dia 12/10/2019</strong>
                            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Tempora recusandae quis aspernatur maiores eveniet perferendis voluptas nisi, ut doloremque corrupti beatae consequuntur provident voluptatibus repellendus. Commodi molestias laudantium et consequuntur!</p>
                        </li>
                        <li class="border border-light p-4 rounded px-2 my-lg-4">
                            <strong class="font-size-lg">Renan Souza - "Excelente!"</strong>
                            <strong class="d-block mb-2">alugou no dia 12/10/2019</strong>
                            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Tempora recusandae quis aspernatur maiores eveniet perferendis voluptas nisi, ut doloremque corrupti beatae consequuntur provident voluptatibus repellendus. Commodi molestias laudantium et consequuntur!</p>
                        </li>
                    </ul>
                    <hr class="my-6">
                    <h3 class="title text-center text-sm-left"><span class="d-block d-sm-inline-block">Deixe seu comentário!</span> <a href="login.php" class="btn btn-secondary mt-3 mt-sm-0 ml-sm-6 va-initial">Faça login</a></h3>
                    <form class="mt-4">
                        <div class="row mx-n1">
                            <div class="col-md-4 p-1">
                                <div class="form-group">
                                    <input type="text" class="form-control border-light" placeholder="Nome" required>
                                </div>
                            </div>
                            <div class="col-md-4 p-1">
                                <div class="form-group">
                                    <input type="text" class="form-control border-light" placeholder="Título" required>
                                </div>
                            </div>
                            <div class="col-md-4 p-1">
                                <div class="form-group">
                                    <label class="label-abs d-lg-none">Data da compra</label>
                                    <input type="date" class="form-control border-light" placeholder="Data da compra" />
                                    <i class="far fa-calendar-alt form-icon text-primary"></i>
                                </div>
                            </div>
                            <div class="col-12 p-1">
                                <div class="form-group">
                                    <textarea class="form-control border-light" placeholder="Comentário" required></textarea>
                                </div>
                            </div>
                            <div class="col-md-4 p-1">
                                <button type="submit" class="btn btn-block btn-secondary">Postar</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-4 pl-lg-6 text-center mt-8 mt-lg-0">
                    <div class="fixed pb-4">
                        <div class="bg-secondary rounded box-shadow p-4">
                            <h3 class="title mb-6">Porque viajar com o Vai de Motorhome?</h3>
                            <ul class="scrollable-md">
                                <li class="px-1 px-md-0 mt-md-4">
                                    <div class="border border-primary border-width-2 p-1 fz-0 d-inline-block rounded-circle">
                                        <div class="ratio-img ratio-img-avatar bg-white rounded-circle">
                                            <i class="fal fa-user-headset"></i>
                                        </div>
                                    </div>
                                    <strong class="d-block my-1">Atendimento personalizado</strong>
                                    <p>Sabemos que viajar de motorhome é novidade para muita gente. Por isso temos uma equipe a disposição para esclarecer qualquer dúvida.</p>
                                </li>
                                <li class="px-1 px-md-0 mt-md-4">
                                    <div class="border border-primary border-width-2 p-1 fz-0 d-inline-block rounded-circle">
                                        <div class="ratio-img ratio-img-avatar bg-white rounded-circle">
                                            <i>R$</i>
                                        </div>
                                    </div>
                                    <strong class="d-block my-1">Pagamento em Reais</strong>
                                    <p>O Pagamento é em reais. Assim você não corre o risco de sofrer com alterações cambiais.</p>
                                </li>
                                <li class="px-1 px-md-0 mt-md-4">
                                    <div class="border border-primary border-width-2 p-1 fz-0 d-inline-block rounded-circle">
                                        <div class="ratio-img ratio-img-avatar bg-white rounded-circle">
                                            <i class="fal fa-rv"></i>
                                        </div>
                                    </div>
                                    <strong class="d-block my-1">Seleção dos melhores Motorhomes</strong>
                                    <p>Selecionamos como frota somente motorhomes que já usamos e aprovamos.</p>
                                </li>
                                <li class="px-1 px-md-0 mt-md-4">
                                    <div class="border border-primary border-width-2 p-1 fz-0 d-inline-block rounded-circle">
                                        <div class="ratio-img ratio-img-avatar bg-white rounded-circle">
                                            <i class="fal fa-users"></i>
                                        </div>
                                    </div>
                                    <strong class="d-block my-1">Feito por quem entende de  viagens de Motorhome</strong>
                                    <p>Nossos fundadores já viajaram em inúmeros modelos de motorhome, tanto que escolheram um para viver em tempo integral.</p>
                                </li>
                                <li class="px-1 px-md-0 mt-md-4">
                                    <div class="border border-primary border-width-2 p-1 fz-0 d-inline-block rounded-circle">
                                        <div class="ratio-img ratio-img-avatar bg-white rounded-circle">
                                            <i class="fal fa-language"></i>
                                        </div>
                                    </div>
                                    <strong class="d-block my-1">Instruções de retirada em português</strong>
                                    <p>Todas as Instruções para retirada do motorhome em Português.</p>
                                </li>
                            </ul>
                        </div>
                        <div class="row justify-content-center mt-6 mx-n1">
                            <div class="col-auto p-1">
                                <span class="badge badge-pill badge-primary">família</span>
                            </div>
                            <div class="col-auto p-1">
                                <span class="badge badge-pill badge-primary">europeu</span>
                            </div>
                            <div class="col-auto p-1">
                                <span class="badge badge-pill badge-primary">casal</span>
                            </div>
                            <div class="col-auto p-1">
                                <span class="badge badge-pill badge-primary">automático</span>
                            </div>
                            <div class="col-auto p-1">
                                <span class="badge badge-pill badge-primary">conforto</span>
                            </div>
                        </div>
                        <hr class="my-6">
                        <h4 class="title mb-4">Ficou alguma dúvida? <br>Fale conosco pelo chat!</h4>
                        <a href="contato.php" class="btn btn-big-icon btn-primary">Entrar em contato</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Modal especificações -->
    <div class="modal fade" id="modal-espec" tabindex="-1" role="dialog" aria-labelledby="modal-espec-title" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="title mb-0" id="modal-espec-title">AS50 Campervan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="far fa-times text-primary"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Earum eligendi ab quis ullam. Soluta praesentium iusto, dolore quidem aliquam facere aliquid quae tempore dolorum, temporibus, ad aspernatur aperiam dolor mollitia? ipsum, dolor sit amet consectetur adipisicing elit. Explicabo, laborum. Praesentium nostrum soluta nesciunt odit laudantium, nihil ipsa maxime sapiente dolore corrupti voluptate minima omnis quos reiciendis corporis modi. Officiis. ipsum, dolor sit amet consectetur adipisicing elit. Nesciunt culpa quod numquam reiciendis! Deserunt non aliquam dolores saepe rem veritatis molestiae consectetur, quaerat error eum omnis, beatae repellendus unde numquam!</p>
                    <hr>
                    <ul class="lista-detalhes row mx-n1">
                        <li class="col-6 col-sm-4 col-md-6 col-lg-3 px-1 d-flex">
                            <i class="fa fa-fw fa-users mr-1"></i>
                            <span>5 passageiros</span>
                        </li>
                        <li class="col-6 col-sm-4 col-md-6 col-lg-3 px-1 d-flex">
                            <i class="fa fa-fw fa-steering-wheel mr-1"></i>
                            <span>Manual</span>
                        </li>
                        <li class="col-6 col-sm-4 col-md-6 col-lg-3 px-1 d-flex">
                            <i class="fa fa-fw fa-faucet-drip mr-1"></i>
                            <span>Pia cozinha</span>
                        </li>
                        <li class="col-6 col-sm-4 col-md-6 col-lg-3 px-1 d-flex">
                            <i class="fa fa-fw fa-snowflake mr-1"></i>
                            <span>Ar condicionado</span>
                        </li>  
                        <li class="col-6 col-sm-4 col-md-6 col-lg-3 px-1 d-flex">
                            <i class="fa fa-fw fa-bed-alt mr-1"></i>
                            <span>5 camas</span>
                        </li>
                        <li class="col-6 col-sm-4 col-md-6 col-lg-3 px-1 d-flex">
                            <i class="far fa-fw fa-th-large mr-1"></i>
                            <span>Cooktop</span>
                        </li>
                        <li class="col-6 col-sm-4 col-md-6 col-lg-3 px-1 d-flex">
                            <i class="fa fa-fw fa-refrigerator mr-1"></i>
                            <span>Refrigerador</span>
                        </li>
                        <li class="col-6 col-sm-4 col-md-6 col-lg-3 px-1 d-flex">
                            <i class="fa fa-fw fa-shower mr-1"></i>
                            <span>Banheiro</span>
                        </li>  
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <?php include('inc_rodape.php'); ?>
</body>
</html>